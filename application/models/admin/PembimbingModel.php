<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PembimbingModel extends CI_Model {
  // Fungsi untuk menampilkan semua data siswa
  public function view(){
    return $this->db->get('tb_pembimbing')->result();
  }

  // Fungsi untuk validasi form tambah dan ubah
  // public function validation($mode){
  //   $this->load->library('form_validation'); // Load library form_validation untuk proses validasinya
  //
  //   // Tambahkan if apakah $mode save atau update
  //   // Karena ketika update, NIS tidak harus divalidasi
  //   // Jadi NIS di validasi hanya ketika menambah data siswa saja
  //   if($mode == "save")
  //
  //   $this->form_validation->set_rules('input_id_pembimbing', 'ID Pembimbing', 'required|max_length[25]');
  //   $this->form_validation->set_rules('input_nama_pembimbing', 'Nama Pembimbing', 'required|max_length[50]');
  //
  //   if($this->form_validation->run()) // Jika validasi benar
  //     return TRUE; // Maka kembalikan hasilnya dengan TRUE
  //   else // Jika ada data yang tidak sesuai validasi
  //     return FALSE; // Maka kembalikan hasilnya dengan FALSE
  // }

  // Fungsi untuk melakukan simpan data ke tabel siswa
  public function save(){
    $data = array(
      "id_pembimbing" => $this->input->post('input_id_pembimbing'),
      "nama_pembimbing" => $this->input->post('input_nama_pembimbing'),

    );

    $this->db->insert('tb_pembimbing', $data); // Untuk mengeksekusi perintah insert data
  }

  public function delete($id_pembimbing){
    $this->db->where('id_pembimbing', $id_pembimbing);
    $this->db->delete('tb_pembimbing'); // Untuk mengeksekusi perintah delete data
  }

  // Fungsi untuk menampilkan data siswa berdasarkan NIS nya
  public function view_by($id_pembimbing){
    $this->db->where('id_pembimbing', $id_pembimbing);
    return $this->db->get('tb_pembimbing')->row();
  }

  // Fungsi untuk melakukan ubah data siswa berdasarkan NIS siswa
  public function edit($id_pembimbing){
    $data = array(
      "nama_pembimbing" => $this->input->post('input_nama_pembimbing')

    );

    $this->db->where('id_pembimbing', $id_pembimbing);
    $this->db->update('tb_pembimbing', $data); // Untuk mengeksekusi perintah update data
  }
}
