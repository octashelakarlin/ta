<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PerusahaanModel extends CI_Model {
  // Fungsi untuk menampilkan semua data siswa
  public function view(){
    return $this->db->get('tb_perusahaan')->result();
  }

  // Fungsi untuk validasi form tambah dan ubah
  public function validation($mode){
    $this->load->library('form_validation'); // Load library form_validation untuk proses validasinya

    // Tambahkan if apakah $mode save atau update
    // Karena ketika update, NIS tidak harus divalidasi
    // Jadi NIS di validasi hanya ketika menambah data siswa saja
    if($mode == "save")

    $this->form_validation->set_rules('input_nama_perusahaan', 'Nama Perusahaan', 'required|max_length[50]');
	$this->form_validation->set_rules('input_alamat', 'Alamat Perusahaan', 'required|max_length[50]');
	$this->form_validation->set_rules('input_kuota', 'Kuota Perusahaan', 'required|numeric| max_length[50]');

    if($this->form_validation->run()) // Jika validasi benar
      return TRUE; // Maka kembalikan hasilnya dengan TRUE
    else // Jika ada data yang tidak sesuai validasi
      return FALSE; // Maka kembalikan hasilnya dengan FALSE
  }

  // Fungsi untuk melakukan simpan data ke tabel siswa
  public function save(){
    $data = array(
      "nama_perusahaan" => $this->input->post('input_nama_perusahaan'),
	  "alamat" => $this->input->post('input_alamat'),
	 "kuota" => $this->input->post('input_kuota')
    );

    $this->db->insert('tb_perusahaan', $data); // Untuk mengeksekusi perintah insert data
  }

  public function delete($id_perusahaan){
    $this->db->where('id_perusahaan', $id_perusahaan);
    $this->db->delete('tb_perusahaan'); // Untuk mengeksekusi perintah delete data
  }

  // Fungsi untuk menampilkan data siswa berdasarkan NIS nya
  public function view_by($id_perusahaan){
    $this->db->where('id_perusahaan', $id_perusahaan);
    return $this->db->get('tb_perusahaan')->row();
  }

  // Fungsi untuk melakukan ubah data siswa berdasarkan NIS siswa
  public function edit($id_perusahaan){
    $data = array(
    "nama_perusahaan" => $this->input->post('input_nama_perusahaan'),
	"alamat" => $this->input->post('input_alamat'),
	"kuota" => $this->input->post('input_kuota')
    );

    $this->db->where('id_perusahaan', $id_perusahaan);
    $this->db->update('tb_perusahaan', $data); // Untuk mengeksekusi perintah update data
  }
}
