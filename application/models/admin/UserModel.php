<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UserModel extends CI_Model {
  // Fungsi untuk menampilkan semua data siswa
  public function view(){
    $this->db->from('tb_user');
    $this->db->join('tb_user_level', 'tb_user_level.id_user_level = tb_user.user_id');
    $query = $this->db->get();

    return $query;

    // $this->db->order_by("user_id", "desc");
    // return $this->db->get('tb_user')->result();
  }

  // Fungsi untuk menampilkan data siswa berdasarkan NIS nya
  public function view_by($user_id){
    $this->db->from('tb_user');
    $this->db->join('tb_user_level', 'tb_user_level.id_user_level = tb_user.user_id');
    $this->db->where('user_id', $user_id);
    $query = $this->db->get();

    if ($query->num_rows() == 1) {
      return $query->row();
    }else {
      return false;
    }
    // return $this->db->get('tb_user')->row();
  }

  // Fungsi untuk validasi form tambah dan ubah
  public function validation($mode){
    $this->load->library('form_validation'); // Load library form_validation untuk proses validasinya

    // Tambahkan if apakah $mode save atau update
    // Karena ketika update, NIS tidak harus divalidasi
    // Jadi NIS di validasi hanya ketika menambah data siswa saja
    if($mode == "save")

    $this->form_validation->set_rules('input_user_email', 'Email', 'required|max_length[50]');
    $this->form_validation->set_rules('input_user_password', 'Password', 'required|max_length[100]');
    $this->form_validation->set_rules('input_user_level', 'Level', 'required|max_length[50]');
    $this->form_validation->set_rules('input_nis', 'NIS', 'max_length[15]');
    $this->form_validation->set_rules('input_id_pembimbing', 'Id Pembimbing', 'max_length[25]');
    $this->form_validation->set_rules('input_hak_akses', 'Hak Akses', 'required');

    if($this->form_validation->run()) // Jika validasi benar
      return TRUE; // Maka kembalikan hasilnya dengan TRUE
    else // Jika ada data yang tidak sesuai validasi
      return FALSE; // Maka kembalikan hasilnya dengan FALSE
  }

  // Fungsi untuk melakukan simpan data ke tabel siswa
  public function save(){
    $data = array(
      "user_email" => $this->input->post('input_user_email'),
      "user_password" => $this->input->post('input_user_password'),
      "user_level" => $this->input->post('input_user_level'),
      "nis" => $this->input->post('input_nis'),
      "id_pembimbing" => $this->input->post('input_id_pembimbing'),
      "hak_akses" => $this->input->post('input_hak_akses')
    );

    $this->db->insert('tb_user', $data); // Untuk mengeksekusi perintah insert data
  }

  // Fungsi untuk melakukan ubah data siswa berdasarkan NIS siswa
  public function edit($user_id){
    $data = array(
      "user_email"      => $this->input->post('input_user_email'),
      "user_password"   => $this->input->post('input_user_password'),
      "user_level"      => $this->input->post('input_user_level'),
      "hak_akses"       => $this->input->post('input_hak_akses')
    );

    $this->db->where('user_id', $user_id);
    $this->db->update('tb_user', $data); // Untuk mengeksekusi perintah update data
  }

  // Fungsi untuk melakukan menghapus data siswa berdasarkan NIS siswa
  public function delete($user_id){
    $this->db->where('user_id', $user_id);
    $this->db->delete('tb_user'); // Untuk mengeksekusi perintah delete data
  }
}
