<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class SiswaModel extends CI_Model {
  
  public function view(){
	$this->db->select('*');
    $this->db->from('tb_siswa');
    $this->db->join('tb_jurusan','tb_jurusan.id_jurusan=tb_siswa.id_jurusan');
    $query = $this->db->get();
    return $query->result();
  }

}
