<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PrakerinModel extends CI_Model {
	
	public function view() 
	{
		$this->db->select('*');
		$this->db->from('tb_prakerin');
		$this->db->join('tb_siswa','tb_siswa.nis=tb_prakerin.nis');
		$this->db->join('tb_jurusan','tb_jurusan.id_jurusan=tb_prakerin.id_jurusan');
		$this->db->join('tb_pembimbing','tb_pembimbing.id_pembimbing=tb_prakerin.id_pembimbing');
		$this->db->join('tb_perusahaan','tb_perusahaan.id_perusahaan=tb_prakerin.id_perusahaan');
		$query = $this->db->get();
		return $query->result();
	}
	
	public function validation($mode)
	{
		$this->load->library('form_validation');
		
		if($mode == "save")
		//$this->form_validation->set_rules('input_id_prakerin', 'ID Prakerin', 'required|max_length[50]');
		$this->form_validation->set_rules('input_nis', 'NIS', 'required');
		$this->form_validation->set_rules('input_nama_siswa', 'Nama Siswa', 'required');
		$this->form_validation->set_rules('input_id_pembimbing', 'ID Pembimbing', 'required');
		$this->form_validation->set_rules('input_id_perusahaan', 'ID Perusahaan', 'required');
		$this->form_validation->set_rules('input_id_jurusan', 'ID Jurusan', 'required');
  
		
		if($this->form_validation->run()) // Jika validasi benar
		  return TRUE; // Maka kembalikan hasilnya dengan TRUE
		else // Jika ada data yang tidak sesuai validasi
		  return FALSE;		
	}
	
	public function save()
	{
		$data = array(
		  "id_prakerin" => $this->input->post('input_id_prakerin'),
		  "nis" => $this->input->post('input_nis'),
		  "nama_siswa" => $this->input->post('input_nama_siswa'),
		  "id_pembimbing" => $this->input->post('input_id_pembimbing'),
		  "id_perusahaan" => $this->input->post('input_id_perusahaan'),
		  "id_jurusan" => $this->input->post('input_id_jurusan')
		);

		$this->db->insert('tb_prakerin', $data);
	}
	
	public function edit($id_prakerin)
	{
	  $data = array(
      "nis" => $this->input->post('input_nis'),
      "nama_siswa" => $this->input->post('input_nama_siswa'),
	  "id_jurusan" => $this->input->post('input_id_jurusan'),
	  "id_pembimbing" => $this->input->post('input_id_pembimbing'),
	  "id_perusahaan" => $this->input->post('input_id_perusahaan')	  
    );

    $this->db->where('id_prakerin', $id_prakerin);
    $this->db->update('tb_prakerin', $data);
	}


	public function view_by($id_prakerin){
    $this->db->where('id_prakerin', $id_prakerin);
    return $this->db->get('tb_prakerin')->row();
  }

  public function delete($id_prakerin){
    $this->db->where('id_prakerin', $id_prakerin);
    $this->db->delete('tb_prakerin'); // Untuk mengeksekusi perintah delete data
  }


	
	public function getNis()
	{
		return $this->db->get("tb_siswa");
	}
	
	public function getPembimbing()
	{
		return $this->db->get("tb_pembimbing");
	}
	
	public function getPerusahaan()
	{
		return $this->db->get("tb_perusahaan");
	}
	
	public function getJurusan()
	{
		return $this->db->get("tb_jurusan");
	}
	
	
  
  
}