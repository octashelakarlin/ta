<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class JurusanModel extends CI_Model {

  public function view(){
    return $this->db->get('tb_jurusan')->result();
  }


  // public function validation($mode){
  //   $this->load->library('form_validation');
  //
  //   if($mode == "save")
  //
  //   $this->form_validation->set_rules('input_nama_jurusan', 'Nama Jurusan', 'required|max_length[50]');
  //
  //   if($this->form_validation->run())
  //     return TRUE;
  //   else
  //     return FALSE;
  // }


  public function save(){
    $data = array(
      "nama_jurusan" => $this->input->post('input_nama_jurusan')
    );

    $this->db->insert('tb_jurusan', $data);
  }

  public function delete($id_jurusan){
    $this->db->where('id_jurusan', $id_jurusan);
    $this->db->delete('tb_jurusan');
  }


  public function view_by($id_jurusan){
    $this->db->where('id_jurusan', $id_jurusan);
    return $this->db->get('tb_jurusan')->row();
  }


  public function edit($id_jurusan){
    $data = array(
      "nama_jurusan" => $this->input->post('input_nama_jurusan')
    );

    $this->db->where('id_jurusan', $id_jurusan);
    $this->db->update('tb_jurusan', $data);
  }
}
