<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AbsensiModel extends CI_Model {


  public function validation($mode)
  {
      $this->load->library('form_validation');
      if($mode == "save")

      $this->form_validation->set_rules('input_nis', 'NIS', 'required|max_length[25]');
      $this->form_validation->set_rules('input_nama_siswa', 'Nama Siswa', 'required|max_length[50]');
      $this->form_validation->set_rules('input_tanggal', 'Tanggal', 'required|max_length[25]');
    	$this->form_validation->set_rules('input_keterangan', 'Keterangan', 'required');

      if($this->form_validation->run())
        return TRUE;
      else
        return FALSE;
  }

  public function save()
  {
      $data = array(
      "nis" => $this->input->post('input_nis'),
      "nama_siswa" => $this->input->post('input_nama_siswa'),
      "tanggal" => $this->input->post('input_tanggal'),
      "keterangan" => $this->input->post('input_keterangan'),

      );

      $this->db->insert('tb_absensi', $data);
  }

 public function view(){
    $this->db->select('*');
    $this->db->from('tb_absensi');
    $this->db->join('tb_siswa','tb_siswa.nis=tb_absensi.nis');
    $query = $this->db->get();
    return $query->result();
  }

  public function getNis()
  {
    return $this->db->get("tb_siswa");
  }




}
