<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AbsensiModel extends CI_Model {


public function validation($mode)
{
    $this->load->library('form_validation');
    if($mode == "save")

    $this->form_validation->set_rules('input_nis', 'NIS', 'required|max_length[25]');
    $this->form_validation->set_rules('input_nama_siswa', 'Nama Siswa', 'required|max_length[50]');
    $this->form_validation->set_rules('input_tanggal', 'Tanggal', 'required|max_length[25]');
	$this->form_validation->set_rules('input_hadir', 'Hadir', 'required|max_length[50]');
	$this->form_validation->set_rules('input_sakit', 'Sakit', 'required|max_length[25]');
	$this->form_validation->set_rules('input_ijin', 'Ijin', 'required|max_length[10]');
	$this->form_validation->set_rules('input_alfa', 'Alfa', 'required|max_length[10]');

    if($this->form_validation->run())
      return TRUE;
    else
      return FALSE;
  }


}
