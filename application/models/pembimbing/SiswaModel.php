<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class SiswaModel extends CI_Model {



  public function view_by_pembimbing()
	{
  // echo "<pre>";
  // print_r($this->session->userdata());die();
    $nama_pembimbing = $this->session->userdata('nama_pembimbing');

		return $this->db->query(
      "SELECT `tb_siswa`.`nis`, `tb_siswa`.`nama_siswa`, `tb_siswa`.`tanggal_lahir`, `tb_siswa`.`alamat`, `tb_siswa`.`jenis_kelamin`, `tb_jurusan`.`nama_jurusan`, `tb_perusahaan`.`nama_perusahaan`,
      `tb_perusahaan`.`alamat`, `tb_perusahaan`.`kuota`, `tb_pembimbing`.`nama_pembimbing`, `tb_prakerin`.`nilai`, `tb_siswa`.`is_active`
      FROM `tb_prakerin`
      JOIN `tb_pembimbing` ON `tb_pembimbing`.`id_pembimbing` = `tb_prakerin`.`id_pembimbing`
      JOIN `tb_perusahaan` ON `tb_perusahaan`.`id_perusahaan` = `tb_prakerin`.`id_perusahaan`
      JOIN `tb_siswa` ON `tb_siswa`.`nis` = `tb_prakerin`.`nis`
      JOIN `tb_jurusan` ON `tb_jurusan`.`id_jurusan` = `tb_siswa`.`id_jurusan`
      WHERE `tb_pembimbing`.`nama_pembimbing`='{$nama_pembimbing}'
      ;")->result();

		// $query = $this->db->get();
    // echo "<pre>";
    // print_r($query);die();
    //
		// return $query->result();
	}

  public function get_absensi()
	{
  // echo "<pre>";
  // print_r($this->session->userdata());die();
    $nama_pembimbing = $this->session->userdata('nama_pembimbing');

		return $this->db->query(
      "SELECT `tb_siswa`.`nis`, `tb_siswa`.`nama_siswa`, `tb_jurusan`.`nama_jurusan`, `tb_perusahaan`.`nama_perusahaan`, `tb_prakerin`.`nilai`, `tb_siswa`.`is_active`
      FROM `tb_prakerin`
      JOIN `tb_pembimbing` ON `tb_pembimbing`.`id_pembimbing` = `tb_prakerin`.`id_pembimbing`
      JOIN `tb_perusahaan` ON `tb_perusahaan`.`id_perusahaan` = `tb_prakerin`.`id_perusahaan`
      JOIN `tb_siswa` ON `tb_siswa`.`nis` = `tb_prakerin`.`nis`
      JOIN `tb_jurusan` ON `tb_jurusan`.`id_jurusan` = `tb_siswa`.`id_jurusan`
      WHERE `tb_pembimbing`.`nama_pembimbing`='{$nama_pembimbing}'
      ;")->result();

		// $query = $this->db->get();
    // echo "<pre>";
    // print_r($query);die();
    //
		// return $query->result();
	}

  public function get_laporan()
	{
  // echo "<pre>";
  // print_r($this->session->userdata());die();
    $nama_pembimbing = $this->session->userdata('nama_pembimbing');

		return $this->db->query(
      "SELECT `tb_siswa`.`nis`, `tb_siswa`.`nama_siswa`, `tb_jurusan`.`nama_jurusan`, `tb_perusahaan`.`nama_perusahaan`, `tb_absensi`.`laporan`, `tb_siswa`.`is_active`
      FROM `tb_prakerin`
      JOIN `tb_pembimbing` ON `tb_pembimbing`.`id_pembimbing` = `tb_prakerin`.`id_pembimbing`
      JOIN `tb_perusahaan` ON `tb_perusahaan`.`id_perusahaan` = `tb_prakerin`.`id_perusahaan`
      JOIN `tb_siswa` ON `tb_siswa`.`nis` = `tb_prakerin`.`nis`
      JOIN `tb_jurusan` ON `tb_jurusan`.`id_jurusan` = `tb_siswa`.`id_jurusan`
      JOIN `tb_absensi` ON `tb_absensi`.`nis` = `tb_siswa`.`nis`
      WHERE `tb_pembimbing`.`nama_pembimbing`='{$nama_pembimbing}'
      ;")->result();

		// $query = $this->db->get();
    // echo "<pre>";
    // print_r($query);die();
    //
		// return $query->result();
	}

  public function get_detail_absen($nis)
	{
  // echo "<pre>";
  // print_r($this->session->userdata());die();
    $nama_pembimbing = $this->session->userdata('nama_pembimbing');

		return $this->db->query(
      "SELECT `tb_absensi`.`id_absensi`, `tb_siswa`.`nis`, `tb_siswa`.`nama_siswa`, `tb_jurusan`.`nama_jurusan`, `tb_perusahaan`.`nama_perusahaan`,
              `tb_absensi`.`created_at`, `tb_absensi`.`laporan`, `tb_siswa`.`is_active`
      FROM `tb_prakerin`
      JOIN `tb_pembimbing` ON `tb_pembimbing`.`id_pembimbing` = `tb_prakerin`.`id_pembimbing`
      JOIN `tb_perusahaan` ON `tb_perusahaan`.`id_perusahaan` = `tb_prakerin`.`id_perusahaan`
      JOIN `tb_siswa` ON `tb_siswa`.`nis` = `tb_prakerin`.`nis`
      JOIN `tb_jurusan` ON `tb_jurusan`.`id_jurusan` = `tb_siswa`.`id_jurusan`
      JOIN `tb_absensi` ON `tb_absensi`.`nis` = `tb_siswa`.`nis`
      WHERE `tb_pembimbing`.`nama_pembimbing`='{$nama_pembimbing}'
      AND `tb_siswa`.`nis`='{$nis}'
      ;")->result();

		// $query = $this->db->get();
    // echo "<pre>";
    // print_r($query);die();
    //
		// return $query->result();
	}

  public function get_nilai()
	{
  // echo "<pre>";
  // print_r($this->session->userdata());die();
    $nama_pembimbing = $this->session->userdata('nama_pembimbing');

		return $this->db->query(
      "SELECT `tb_siswa`.`nis`, `tb_siswa`.`nama_siswa`, `tb_jurusan`.`nama_jurusan`, `tb_perusahaan`.`nama_perusahaan`, `tb_prakerin`.`nilai`, `tb_siswa`.`is_active`
      FROM `tb_prakerin`
      JOIN `tb_pembimbing` ON `tb_pembimbing`.`id_pembimbing` = `tb_prakerin`.`id_pembimbing`
      JOIN `tb_perusahaan` ON `tb_perusahaan`.`id_perusahaan` = `tb_prakerin`.`id_perusahaan`
      JOIN `tb_siswa` ON `tb_siswa`.`nis` = `tb_prakerin`.`nis`
      JOIN `tb_jurusan` ON `tb_jurusan`.`id_jurusan` = `tb_siswa`.`id_jurusan`
      WHERE `tb_pembimbing`.`nama_pembimbing`='{$nama_pembimbing}'
      ;")->result();

		// $query = $this->db->get();
    // echo "<pre>";
    // print_r($query);die();
    //
		// return $query->result();
	}

}
