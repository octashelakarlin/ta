<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class PembimbingModel extends CI_Model {
  
  public function view(){
    return $this->db->get('tb_pembimbing')->result();
  }

  public function getPembimbing($keyword)
  {
  	$this->db->select('*');
  	$this->db->from('tb_pembimbing');
  	$this->db->like('id_pembimbing',$keyword);
  	$this->db->or_like('nama_pembimbing',$keyword);
  	return $this->db->get()->result();
  }

}
