<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SiswaModel extends CI_Model {

  public function validation($mode){
    $this->load->library('form_validation');
    if($mode == "save")

    $this->form_validation->set_rules('input_nis', 'NIS', 'required|max_length[25]');
    $this->form_validation->set_rules('input_nama_siswa', 'Nama Siswa', 'required|max_length[50]');
    $this->form_validation->set_rules('input_tanggal_lahir', 'Tanggal Lahir', 'required|max_length[25]');
  	$this->form_validation->set_rules('input_alamat', 'Alamat', 'required|max_length[50]');
  	$this->form_validation->set_rules('input_jenis_kelamin', 'Jenis Kelamin', 'required|max_length[25]');
  	$this->form_validation->set_rules('input_id_jurusan', 'Id Jurusan', 'required|max_length[10]');

    if($this->form_validation->run())
      return TRUE;
    else
      return FALSE;
  }

  public function save(){
    $data = array(
      "nis" => $this->input->post('input_nis'),
      "nama_siswa" => $this->input->post('input_nama_siswa'),
      "tanggal_lahir" => $this->input->post('input_tanggal_lahir'),
  	  "alamat" => $this->input->post('input_alamat'),
  	  "jenis_kelamin" => $this->input->post('input_jenis_kelamin'),
  	  "id_jurusan" => $this->input->post('input_id_jurusan')

    );

    $this->db->insert('tb_siswa', $data); // Untuk mengeksekusi perintah insert data
  }

  public function edit($nis){
    $data = array(
      "nama_siswa" => $this->input->post('input_nama_siswa'),
      "tanggal_lahir" => $this->input->post('input_tanggal_lahir'),
  	  "alamat" => $this->input->post('input_alamat'),
  	  "jenis_kelamin" => $this->input->post('input_jenis_kelamin'),
  	  "id_jurusan" => $this->input->post('input_id_jurusan')
    );

    $this->db->where('nis', $nis);
    $this->db->update('tb_siswa', $data);
  }


   public function getJurusan(){
    return $this->db->get("tb_jurusan");
  }


  public function view(){
    $this->db->select('*');
		$this->db->from('tb_siswa');
		$this->db->join('tb_jurusan','tb_jurusan.id_jurusan=tb_siswa.id_jurusan');
		$query = $this->db->get();
		return $query->result();
  }

  public function get_siswa($nis){
    $this->db->from('tb_siswa');
    $this->db->join('tb_jurusan','tb_jurusan.id_jurusan=tb_siswa.id_jurusan');
    $this->db->where('nis',$nis);
    // $this->db->or_where('username',$emailUsername);
    // fetch seluruh hasil pada db berupa array
    return $this->db->get()->row();
  }

}
