<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PrakerinModel extends CI_Model {

	public function view()
	{
		return $this->db->query(
      "SELECT `prkrn`.`id_prakerin`, `ssw`.`nis`, `ssw`.`nama_siswa`, `jrsn`.`nama_jurusan`, `pmbmbng`.`nama_pembimbing`,
							`prshn`.`nama_perusahaan`, `prkrn`.`laporan`, `prkrn`.`nilai`, `ssw`.`is_active`
      FROM `tb_prakerin` AS `prkrn`
			JOIN `tb_siswa` AS `ssw` ON `ssw`.`nis` = `prkrn`.`nis`
      JOIN `tb_pembimbing` AS `pmbmbng` ON `pmbmbng`.`id_pembimbing` = `prkrn`.`id_pembimbing`
      JOIN `tb_perusahaan` AS `prshn` ON `prshn`.`id_perusahaan` = `prkrn`.`id_perusahaan`
      JOIN `tb_jurusan` AS `jrsn` ON `jrsn`.`id_jurusan` = `ssw`.`id_jurusan`
      WHERE `prkrn`.`nis`='{$this->session->userdata("nis")}'
    ;")->result();

	}

// 
	public function getPrakerin($keyword)
  {
  	$this->db->select('*');
  	$this->db->from('tb_prakerin');
  	$this->db->like('id_prakerin',$keyword);
  	$this->db->or_like('nis',$keyword);
  	$this->db->or_like('nama_siswa',$keyword);
  	$this->db->join('tb_jurusan','tb_jurusan.id_jurusan=tb_prakerin.id_jurusan');
		$this->db->join('tb_pembimbing','tb_pembimbing.id_pembimbing=tb_prakerin.id_pembimbing');
		$this->db->join('tb_perusahaan','tb_perusahaan.id_perusahaan=tb_prakerin.id_perusahaan');
  	return $this->db->get()->result();
  }
}
