<?php

/**
 *
 */
class DaftarModel extends CI_Model
{

  function daftar($user_email,$user_password,$user_level,$nis,$id_pembimbing)
  {
    if ($user_level == "Admin") {
      $this->db->select('*');
      $this->db->from('tb_admin');
      $this->db->where('user_email',$user_email);
      $querycek = $this->db->get();

      if ($querycek->num_rows()==0) {
        $data = array(
          "user_email" => $user_email,
          "user_password" => $user_password,
          "user_verifikasi" => 0
        );

        $query = $this->db->insert('tb_admin', $data); // Untuk mengeksekusi perintah insert data

        if ($query) {
          return true;
        } else {
          return false;
        }

      } else {
        return false;
      }

    } elseif ($user_level == "Siswa") {
      $this->db->select('*');
      $this->db->from('tb_user');
      $this->db->or_where('user_email',$user_email);
      $this->db->or_where('nis',$nis);
      $querycek = $this->db->get();

      if ($querycek->num_rows()==0) {
        $data = array(
          "user_email" => $user_email,
          "user_password" =>$user_password,
          "user_level" => $user_level,
          "nis" => $nis,
          "id_pembimbing" => $id_pembimbing,
          "hak_akses" => 0
        );
        $data2 = array(
          "nis" => $nis,
        );

        $query = $this->db->insert('tb_user', $data); // Untuk mengeksekusi perintah insert data
        $query2 = $this->db->insert('tb_siswa', $data2); // Untuk mengeksekusi perintah insert data

        if ($query) {
          return true;
        } else {
          return false;
        }

      } else {
        return false;
      }



    }
  }

}
