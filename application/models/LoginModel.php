<?php

class LoginModel extends CI_Model
{

  public function login($user_email,$user_password,$user_level)
  {

    if ($user_level == null) {
      redirect();
    }

    if ($user_level == 'Superadmin') {
      $this->db->from('tb_superadmin');
      $this->db->join('tb_user', 'tb_user.user_id = tb_superadmin.user_id');
      $this->db->where('user_email', $user_email);
      $this->db->where('user_password', $user_password);
      $query = $this->db->get();
      // echo "<pre>";
      // print_r($query->row());die();
      if ( $query->num_rows() == 1) {
        return $query;
      }
      return false;

    } elseif ($user_level == 'Admin') {
      $this->db->from('tb_admin');
      $this->db->join('tb_user', 'tb_user.user_id = tb_admin.user_id');
      $this->db->join('tb_user_level', 'tb_user_level.id_user_level = tb_user.user_id');
      $this->db->where('user_email', $user_email);
      $this->db->where('user_password', $user_password);
      $query = $this->db->get();
      // echo "<pre>";
      // print_r($query->row());die();
      if ( $query->num_rows() == 1) {
        return $query;
      }
      return false;

    } elseif ($user_level == "Siswa" || $user_level == "Pembimbing" || $user_level == "Perusahaan") {
        if ($user_level == "Siswa") {
          // $query = $this->db->query(
          //   "SELECT *
          //   FROM `tb_siswa`
          //   JOIN `tb_user` ON `tb_user`.`user_id` = `tb_siswa`.`user_id`
          //   JOIN `tb_siswa` ON `tb_siswa`.`id_jurusan` = `tb_jurusan`.`id_jurusan`
          //   JOIN `tb_user_level` ON `tb_user_level`.`id_user_level` = `tb_user`.`user_level`
          //   WHERE `tb_user`.`user_email`='{$user_email}'
          //   AND `tb_user`.`user_password`='{$user_password}'
          //   ;")->result();

          $this->db->from('tb_siswa');
          $this->db->join('tb_user', 'tb_user.user_id = tb_siswa.user_id');
          $this->db->join('tb_jurusan', 'tb_jurusan.id_jurusan = tb_siswa.id_jurusan');
          $this->db->join('tb_user_level', 'tb_user_level.id_user_level = tb_user.user_level');
          $this->db->where('user_email', $user_email);
          $this->db->where('user_password', $user_password);
          $query = $this->db->get();
          if ( $query->num_rows() == 1) {
            return $query;
          }
          return false;

        } elseif ($user_level == "Pembimbing") {
          $this->db->from('tb_pembimbing');
          $this->db->join('tb_user', 'tb_user.user_id = tb_pembimbing.user_id');
          $this->db->join('tb_user_level', 'tb_user_level.id_user_level = tb_user.user_level');
          // $this->db->join('tb_siswa', 'tb_siswa.user_id = tb_user.user_id');
          // $this->db->join('tb_jurusan', 'tb_jurusan.id_jurusan = tb_siswa.id_jurusan');
          $this->db->where('user_email', $user_email);
          $this->db->where('user_password', $user_password);
          $query = $this->db->get();
          // echo "<pre>";
          // print_r($query->row());die();
          if ( $query->num_rows() == 1) {
            return $query;
          }
          return false;

        } elseif ($user_level == "Perusahaan") {
          $this->db->from('tb_perusahaan');
          $this->db->join('tb_user', 'tb_user.user_id = tb_perusahaan.user_id');
          $this->db->join('tb_user_level', 'tb_user_level.id_user_level = tb_user.user_level');
          $this->db->where('user_email', $user_email);
          $this->db->where('user_password', $user_password);
          $query = $this->db->get();
          // echo "<pre>";
          // print_r($query->row());die();
          if ( $query->num_rows() == 1) {
            return $query;
          }
          return false;
        }
        die('deqwqwdedweq');

        $querycek = $this->db->get();

        if ($querycek->num_rows()==1) {
            return $querycek->result();
        } else {
            $this->db->select('*');
            $this->db->from('tb_user');
            // $this->db->join('tb_dosen','tb_dosen.id_dosen=tb_user.id_dosen');
            $this->db->where('user_email',$user_email);
            $this->db->where('user_password',$user_password);
            $this->db->limit(1);

            $query = $this->db->get();

            if ($query->num_rows()==1) {
              return $query->result();
            } else {
              return false;
            }
        }
    }

	}
}
