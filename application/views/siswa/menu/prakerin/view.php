<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid box-warning">
      <div class="box-header">
        <center><h3 class="box-title">Data Prakerin</h3></center>
      </div>

      <div class="box-body table-responsive no-padding">
          <!-- <div class="navbar-form">
            <?php echo form_open('siswa/prakerincontroller/search') ?>
              <input type="text" name="keyword" class="form-control" placeholder="Search">
              <button type="submit" class="btn btn-success">Cari</button>
            <?php echo form_close()?>
          </div> -->
        <table class="table table-striped">

          <tr>
            <th>ID</th>
            <th>NIS</th>
            <th>Nama Siswa</th>
      			<th>Jurusan</th>
      			<th>Nama Pembimbing</th>
      			<th>Nama Perusahaan</th>
      			<th><center>Laporan</center></th>
      			<th>Nilai Akhir</th>
            <!-- <th><center>Aksi</center></th> -->
          </tr>

          <?php
          if( ! empty($prakerin)){
            foreach($prakerin as $data){
              $id_prakerin = rawurlencode($data->id_prakerin);
              echo
              "<tr>
                <td>".$data->id_prakerin."</td>
                <td>".$data->nis."</td>
                <td>".$data->nama_siswa."</td>
        			  <td>".$data->nama_jurusan."</td>
        			  <td>".$data->nama_pembimbing."</td>
        			  <td>".$data->nama_perusahaan."</td>
                <td><a href= ".base_url("file/{$data->laporan}")." > <button class='btn btn-block btn-success btn-xs' type='button'> Download </button> </a></td>
                <td>".$data->nilai."</td>
                ".""
                // .<td><a href='".base_url("/siswa/prakerincontroller/ubah/".$id_prakerin)."'><button class='btn btn-block btn-success btn-xs' type='button' >Edit</button></a></td>.
                ."
              </tr>";
            }
          }else{
            echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
          }
          ?>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
