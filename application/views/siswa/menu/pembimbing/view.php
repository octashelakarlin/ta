<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid box-warning">
      <div class="box-header">
        <center><h3 class="box-title">Daftar Pembimbing</h3></center>
      </div>
  
      <div class="box-body table-responsive no-padding">
        <div class="navbar-form">
          <?php echo form_open('siswa/pembimbingcontroller/search') ?>
          <input type="text" name="keyword" class="form-control"
          placeholder="Search">
          <button type="submit" class="btn btn-success">Cari</button>
          <?php echo form_close()?>
          </div>

        <table class="table table-striped">
          <tr>
            <th>NIP</th>
            <th>Nama Pembimbing</th>
          </tr>

          <?php
          if( ! empty($pembimbing)){
            foreach($pembimbing as $data){
              $id_pembimbing = rawurlencode($data->id_pembimbing);
              echo "<tr>
              <td>".$data->id_pembimbing."</td>
              <td>".$data->nama_pembimbing."</td> 
              </tr>";
            }
          }else{
            echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
          }
          ?>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
