<div class="row">
  <div class="col-xs-12">
    <div class="box box-warning">
      <div class="box-header  with-border">
        <center><h3 class="box-title">Input Data Siswa</h3></center>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <body>
          <div style="color: red;"><?php echo validation_errors(); ?></div>
          <?php echo form_open("siswa/siswacontroller/tambah"); ?>
            <table cellpadding="8">

			        <tr>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"> Input NIS</label>
                  <input type="text" class="form-control" name="input_nis" id="inputSuccess" placeholder="NIS" value="<?php echo set_value('input_nis'); ?>">
                </div>
              </tr>

              <tr>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"> Input Nama Siswa</label>
                  <input type="text" class="form-control" name="input_nama_siswa" id="inputSuccess" placeholder="Nama Siswa" value="<?php echo set_value('input_nama_siswa'); ?>">
                </div>
              </tr>

              <tr>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"> Input Tanggal Lahir</label>
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="date" name="input_tanggal_lahir" class="form-control" id="datepicker">
                  </div>
                  <!-- <input type="text" class="form-control" name="input_tanggal_lahir" id="inputSuccess" placeholder="yyyy-MM-DD" value="<?php echo set_value('input_tanggal_lahir'); ?>"> -->
                </div>
              </tr>

			   <tr>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"> Alamat</label>
                  <textarea class="form-control" name="input_alamat" rows="3" placeholder="Enter ..."><?php echo set_value('input_alamat'); ?></textarea>
                </div>
              </tr>


              <tr>
                <!-- /.box -->
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"> Jenis Kelamin</label>
                  <div class="box box-solid box-success">
                    <!-- /.box-header -->
                    <div class="box-body">
                      <div class="radio">
                        <label >
                          <input type="radio" name="input_jenis_kelamin" id="optionsRadios1" value="Laki-laki" <?php echo set_radio('jeniskelamin', 'Laki-laki'); ?>> Laki-laki
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="input_jenis_kelamin" id="optionsRadios2" value="Perempuan" <?php echo set_radio('jeniskelamin', 'Perempuan'); ?>> Perempuan
                        </label>
                      </div>
                    </div>
					</div>
					</div>
              </tr>

			 <tr>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess">Id Jurusan</label>
                  <select name="input_id_jurusan" id="id_jurusan" required class="form-control">
                    <option value="" disabled selected>Pilih Jurusan</option>
                    <?php
                     foreach ($jurusan->result() as $data) {
                     echo "<option value='".$data->id_jurusan."'>".$data->nama_jurusan."</option>";
                     }
                     ?>
                  </select>
                </div>
              </tr>

            </table>

            <input type="submit" class="btn btn-block btn-success" name="submit" value="Simpan">

            <hr>
            <a href="<?php echo base_url('/siswa/siswacontroller/'); ?>"><input class="btn btn-block btn-danger" type="button" value="Batal"></a>
          <?php echo form_close(); ?>
        </body>

      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->


    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
