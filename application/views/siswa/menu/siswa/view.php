<!-- Main content -->
<section class="content">
<?php

  // echo "<pre>";
  // print_r($siswa);
  // echo "</pre>";die();

 ?>
  <!-- Default box -->
  <div class="box">
    <div class="box-body" style="padding:3%;">
      <table width='90%' align='center'>
        <tr>
          <td width='30%'>NIS</td>
          <td width='2%'>:</td>
          <td><?php echo $siswa->nis ?></td>
        </tr>
        <tr>
          <td>Nama Siswa</td>
          <td>:</td>
          <td><?php echo $siswa->nama_siswa ?></td>
        </tr>
        <tr>
          <td>Tanggal Lahir</td>
          <td>:</td>
          <td><?php echo $siswa->tanggal_lahir ?></td>
        </tr>
        <tr>
          <td>Alamat</td>
          <td>:</td>
          <td><?php echo $siswa->alamat ?></td>
        </tr>
        <tr>
          <td>Jenis Kelamin</td>
          <td>:</td>
          <td><?php echo $siswa->jenis_kelamin ?></td>
        </tr>
        <tr>
          <td>Jurusan</td>
          <td>:</td>
          <td><?php echo $siswa->nama_jurusan ?></td>
        </tr>
      </table>

    </div>
    <!-- /.box-body -->
    <div class="box-footer" style="padding:2%;">
      <div style="width:60%; margin:auto;">
        <a href="<?php echo base_url("siswa/siswacontroller/ubah/{$siswa->nis}"); ?>"><input class="btn btn-block btn-success" type="button" value="Edit Data"></a> 

       <!-- <button style="width:100%;" class="btn btn-success rounded btn-md" type="button" name="editSiswa" 
        onclick="location.href='<?php echo base_url("siswa/siswacontroller/ubah/{$siswa->nis}"); ?>';">Edit Data</button> -->
      </div>
    </div>
    <!-- /.box-footer-->
  </div>
  <!-- /.box -->

</section>
<!-- /.content -->
