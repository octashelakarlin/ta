<div class="row">
  <div class="col-xs-12">
    <div class="box box-warning">
      <div class="box-header  with-border">
        <center><h3 class="box-title">Input Data Laporan</h3></center>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <body>
          <div style="color: red;"><?php echo validation_errors(); ?></div>
          <!-- <?php echo form_open("siswa/laporancontroller/tambah"); ?> -->
          <!-- <?php echo form_open_multipart('siswa/laporancontroller/upload_file');?>
            <input type="submit" class="btn btn-block btn-success" name="submit" value="Simpan">

          </form> -->
          <!-- <?php echo form_close(); ?> -->
          <table cellpadding="8">
            <tr>
              <div class="form-group has-success">
                <label class="control-label" for="inputSuccess">NIS</label>
                <input type="text" class="form-control" name="input_nis" id="inputSuccess" placeholder="NIS" value="<?php echo $laporan->nis; ?>" disabled>
              </div>
            </tr>
            <tr>
              <div class="form-group has-success">
                <label class="control-label" for="inputSuccess">Pembimbing</label>
                <input type="text" class="form-control" name="input_pembimbing" id="inputSuccess" placeholder="Pembimbing" value="<?php echo $laporan->nama_pembimbing; ?>" disabled>
              </div>
            </tr>
            <tr>
              <div class="form-group has-success">
                <label class="control-label" for="inputSuccess">Laporan</label>
                <form action="<?php echo base_url('siswa/laporancontroller/upload_file') ?>" method="post" enctype="multipart/form-data">
                  <input type="file" name="laporan" id="laporan">
                  <br>
                  <input type="submit" name="upload" value="Upload" class="btn btn-block btn-success">
                </form>
              </div>
            </tr>
            <hr>
            <a href="<?php echo base_url('/siswa/laporancontroller/'); ?>"><input class="btn btn-block btn-danger" type="button" value="Batal"></a>

          </table>

        </body>
