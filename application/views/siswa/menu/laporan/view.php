

<!-- Main content -->
<section class="content">
<?php

  // echo "<pre>";
  // print_r($siswa);
  // echo "</pre>";die();

 ?>
  <!-- Default box -->
  <div class="box">
    <div class="box-body" style="padding:3%;">
      <table width='90%' align='center'>
        <tr>
          <td width='30%'>NIS</td>
          <td width='2%'>:</td>
          <td><?php echo $field_laporan->nis ?></td>
        </tr>
        <tr>
          <td>Nama Siswa</td>
          <td>:</td>
          <td><?php echo $field_laporan->nama_siswa ?></td>
        </tr>
        <tr>
          <td>Nama Jurusan</td>
          <td>:</td>
          <td><?php echo $field_laporan->nama_jurusan ?></td>
        </tr>
        <tr>
          <td>Nama Pembimbing</td>
          <td>:</td>
          <td><?php echo $field_laporan->nama_pembimbing ?></td>
        </tr>
        <tr>
          <td>Nama Perusahaan</td>
          <td>:</td>
          <td><?php echo $field_laporan->nama_perusahaan ?></td>
        </tr>
        <tr>
          <td>Nilai Akhir</td>
          <td>:</td>
          <td><?php echo $field_laporan->nilai ?></td>
        </tr>
      </table>

    </div>
    <!-- /.box-body -->
    <!-- <div class="box-footer" style="padding:2%;">
      <div style="width:60%; margin:auto;">
        <a href="<?php echo base_url("siswa/siswacontroller/ubah/{$field_laporan->nis}"); ?>"><input class="btn btn-block btn-success" type="button" value="Edit Data"></a> -->

       <!-- <button style="width:100%;" class="btn btn-success rounded btn-md" type="button" name="editSiswa"
        onclick="location.href='<?php echo base_url("siswa/siswacontroller/ubah/{$field_laporan->nis}"); ?>';">Edit Data</button> -->
      <!-- </div>
    </div> -->
    <!-- /.box-footer-->
  </div>
  <!-- /.box -->

</section>
<!-- /.content -->



<div class="row">
  <div class="col-xs-12">
    <a href='<?php echo base_url("siswa/laporancontroller/tambah"); ?>'><input class="btn btn-block btn-success btn-xs" type="button" value="Tambah"></a>
    <div class="box box-solid box-warning">
      <div class="box-header">
        <center><h3 class="box-title">Daftar Laporan per Absensi</h3></center>
        <div class="box-tools">
        </div>
      </div>
      <!-- /.box-header -->

      <div class="box-body table-responsive no-padding">

        <table class="table table-striped">
          <tr>
            <th>ID Laporan</th>
			      <th>Laporan</th>
            <th>Waktu</th>
            <th>Tanggal</th>
            <th colspan="2">Aksi</th>
          </tr>
          <?php
          // echo "<pre>";
          // print_r($laporan);die();
          if( ! empty($laporan)){ // Jika data siswa tidak sama dengan kosong, artinya jika data siswa ada
            foreach($laporan as $data){
              $createdAt = explode(' ', $data->created_at);
              $waktu = $createdAt[1];
              $tanggal = $createdAt[0];
              echo
              "<tr>
  		           <td>".$data->id_absensi."</td>
                <td>".$data->laporan."</td>
                <td>".$waktu."</td>
                <td>".$tanggal."</td>

                <td><a href='".base_url("file/".$data->laporan)."'><button class='btn btn-block btn-success btn-xs' type='button' > Download </button></a></td>

              </tr>";
            }
          }else{ // Jika data siswa kosong
            echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
          }
          ?>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
