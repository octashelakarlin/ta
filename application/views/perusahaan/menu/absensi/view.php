<div class="row">
  <div class="col-xs-12">
    <a href='<?php echo base_url("perusahaan/absensicontroller/tambah"); ?>'><input class="btn btn-block btn-success btn-xs" type="button" value="Tambah"></a>
    <div class="box box-solid box-warning">

      <div class="box-header">

        <center><h3 class="box-title">Daftar Absensi Siswa</h3></center>

        <div class="box-tools">

        </div>
      </div>
      <!-- /.box-header -->

      <div class="box-body table-responsive no-padding">

        <table class="table table-striped">
          <tr>
            <th>NIS</th>
            <th>Nama Siswa</th>
            <th>Tanggal</th>
            <th>Keterangan</th>
            <th>Aksi</th>
          </tr>
          <?php
          if( ! empty($jurusan)){ // Jika data siswa tidak sama dengan kosong, artinya jika data siswa ada
            foreach($jurusan as $data){
              echo "<tr>
              <td>".$data->nis."</td>
              <td>".$data->nama_siswa."</td>
              <td>".$data->tanggal."</td>
              <td>".$data->keterangan."</td>

              <td><a href='".base_url("/perusahaan/absensicontroller/ubah/".$data->id_jurusan)."'><button class='btn btn-block btn-success btn-xs' type='button' >Edit</button></a></td>
              <td><a href='".base_url("/perusahaan/absensicontroller/hapus/".$data->id_jurusan)."'><button class='btn btn-block btn-danger btn-xs' type='button' >Hapus</button></a></td>
              </tr>";
            }
          }else{ // Jika data siswa kosong
            echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
          }
          ?>
        </table>
      </div>
      <!-- /.box-body -->
    </div>

    <!-- /.box -->


    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
