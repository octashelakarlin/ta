<header class="main-header">
  <!-- Logo -->
  <a href="" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>T</b>A</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><i class="fa fa-graduation-cap"></i>PRAKERIN</span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>

    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- Messages: style can be found in dropdown.less-->

        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="<?php echo base_url('assets/template/back/dist') ?>/img/user_example.png" class="user-image" alt="User Image">
            <span class="hidden-xs">
              <?php
              if ($this->session->userdata('nama_pembimbing')!="") {
                  echo $this->session->userdata('nama_pembimbing');
              } else {
                  echo $this->session->userdata('user_email');
              }
            ?></span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <img src="<?php echo base_url('assets/template/back/dist') ?>/img/user_example.png" class="img-circle" alt="User Image">

              <p>
                <?php
                  if ($this->session->userdata('nama_pembimbing')!="") {
                      echo $this->session->userdata('nama_pembimbing');
                  } else {
                      echo $this->session->userdata('user_email');
                  }
                ?>
               <small><?php echo $this->session->userdata('user_email'); ?></small>
			   <small><?php echo $this->session->userdata('id_pembimbing'); ?></small>
              </p>
            </li>
            <!-- Menu Body -->

            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-right">
                <a href="<?php echo base_url().'/logincontroller/logout' ?>" class="btn btn-default btn-flat">Keluar</a>
              </div>
            </li>
          </ul>
        </li>
        <!-- Control Sidebar Toggle Button -->

      </ul>
    </div>
  </nav>
</header>
