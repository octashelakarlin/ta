<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo base_url('assets/template/back/dist') ?>/img/user_example.png" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>
          <?php
            if ($this->session->userdata('nama_perusahaan')!="") {
                echo $this->session->userdata('nama_perusahaan');
            } else {
                echo $this->session->userdata('user_email');
            }
          ?>
        </p>
        <a href="#"><i class="fa fa-circle text-success"></i>Perusahaan</a>
      </div>
    </div>
    <!-- search form -->
    <form hidden action="#" method="get" class="sidebar-form">
      <div class="input-group">
        <input type="text" name="q" class="form-control" placeholder="Search...">
        <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
      </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MENU</li>
      <li <?php if($header == "Daftar Absensi Siswa") {echo "class=active";} ?>>
        <a href="<?php echo base_url(''); ?>./perusahaan/absensicontroller/">
          <i class="glyphicon glyphicon-th-large"></i> <span>Daftar Absensi Siswa</span>
          <span class="pull-right-container">
          </span>
        </a>
      </li>
	
	  <li <?php if($header == "Data Prakerin") {echo "class=active";} ?>>
        <a href="<?php echo base_url(''); ?>./pembimbing/absensicontroller/">
          <i class="glyphicon glyphicon-th-large"></i> <span>Absensi Siswa</span>
          <span class="pull-right-container">
          </span>
        </a>
      </li>
	  
	  <li <?php if($header == "Data Pembimbing") {echo "class=active";} ?>>
        <a href="<?php echo base_url(''); ?>./pembimbing/laporancontroller/">
          <i class="glyphicon glyphicon-th-large"></i><span>Laporan Siswa </span>
          <span class="pull-right-container">
          </span>
        </a>
      </li>
	  
	 <li <?php if($header == "Laporan") {echo "class=active";} ?>>
        <a href="<?php echo base_url(''); ?>./pembimbing/nilaicontroller/">
          <i class="glyphicon glyphicon-th-large"></i> <span>Nilai Siswa</span>
          <span class="pull-right-container">
          </span>
        </a>
      </li>


      <li hidden class="header">LABELS</li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
