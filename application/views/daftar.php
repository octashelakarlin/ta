<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>PRAKERIN</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/template/back/bower_components') ?>/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('assets/template/back/bower_components') ?>/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('assets/template/back/bower_components') ?>/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('assets/template/back/dist') ?>/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url('assets/template/back/plugins') ?>plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition register-page">
<div class="register-box">
  <div class="register-logo">
    <a href=<?php echo base_url(); ?>>DAFTAR AKUN</a>
  </div>

  <div class="register-box-body">


		 <?php echo form_open('daftarcontroller/proses_daftar'); ?>
          <div class="form-group has-feedback">
            <input name="input_user_email" type="email" class="form-control" placeholder="Email" required>
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>

          <div class="form-group has-feedback">
            <input name="input_user_password" type="password" class="form-control" placeholder="Password" required>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>

          <div class="form-group has-feedback">
            <select name='input_user_level' id='select-level' class="form-control" required>
              <option value="" disabled selected>Daftar Sebagai</option>
              <option value="Siswa">Siswa</option>
              <option value="Admin">Admin</option>
            </select>
          </div>

          <div id="div-nis" class="form-group has-feedback">
            <input name="input_nis" type="text" class="form-control" placeholder="NIS" >
            <span class="fa fa-black-tie form-control-feedback"></span>
          </div>


          <div class="row">
            <div class="col-xs-8">

            </div>
            <!-- /.col -->
            <div class="col-xs-4">
              <input type="submit" value="Daftar" class="btn btn-primary btn-block btn-flat"></input>
            </div>
            <!-- /.col -->
          </div>
		<?php echo form_close(); ?>

    <script src="<?php echo base_url("js/jquery.min.js"); ?>" type="text/javascript"></script>

    <script>
    $(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
      $("#div-nis").hide();
      $("#div-nip").hide();
      // Kita sembunyikan dulu untuk loadingnya
      // $("#loading").hide();

      $("#select-level").change(function(){ // Ketika user mengganti atau memilih data provinsi
        // $("#kota").hide(); // Sembunyikan dulu combobox kota nya
        // $("#loading").show(); // Tampilkan loadingnya
        var level = $("#select-level").val();
        if (level == "Siswa")
        {
            $("#div-nis").show();
            $("#div-nip").hide();
        }
        else if (level == "Pembimbing")
        {
            $("#div-nip").show();
            $("#div-nis").hide();
        }
        else if (level == "Admin")
        {
            $("#div-nis").hide();
            $("#div-nip").hide();
        }

      });
    });
    </script>

    <div class="social-auth-links text-center">
      <?php
      if (isset($pesan)) {
        echo "<script>alert('$pesan')</script";
      }
       ?>
      <p>- OR -</p>

      <a href="<?php echo base_url(''); ?>./logincontroller" class="text-center">Masuk</a>
    </div>


  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 3 -->
<script src="<?php echo base_url('assets/template/back/bower_components') ?>/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url('assets/template/back/bower_components') ?>/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url('assets/template/back/plugins') ?>/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
