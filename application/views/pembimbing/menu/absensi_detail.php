<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid box-warning">
      <div class="box-header">
        <center><h3 class="box-title">Daftar Laporan <br> <?php echo "{$siswa[0]->nama_siswa} ({$siswa[0]->nis})" ?></h3></center>
      </div>


      <div class="box-body table-responsive no-padding">
        <table class="table table-striped">

          <tr>
            <th>ID Absensi</th>
            <th>Tanggal</th>
            <th>Waktu</th>
            <th><center>File Laporan</center></th>
          </tr>

          <?php
          // echo "<pre>";
          // print_r($siswa);die();
          if( !empty($siswa))
          {
            foreach($siswa as $data)
            {
              $createdAt = explode(' ', $data->created_at);
              echo "<tr>
              <td>".$data->id_absensi."</td>
              <td>".$createdAt[0]."</td>
              <td>".$createdAt[1]."</td>
              <td>
                <a target=_blank href= ".base_url("file/{$data->laporan}")." > <button class='btn btn-block btn-success btn-xs' type='button' >Lihat</button> </a>
              </td>
			          </tr>";
            }
          }else{
            echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
          }

          ?>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
