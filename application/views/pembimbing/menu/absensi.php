<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid box-warning">
      <div class="box-header">
        <center><h3 class="box-title">Daftar Absensi Siswa Bimbingan</h3></center>
      </div>


      <div class="box-body table-responsive no-padding">
        <table class="table table-striped">

          <tr>
            <th>NIS</th>
            <th>Nama Siswa</th>
            <th>Jurusan</th>
            <th>Perusahaan</th>
            <th><center>Rekap Absensi</center></th>
          </tr>

          <?php
          // echo "<pre>";
          // print_r($siswa);die();
          if( !empty($siswa))
          {
            foreach($siswa as $data)
            {
              echo "<tr>
              <td>".$data->nis."</td>
              <td>".$data->nama_siswa."</td>
              <td>".$data->nama_jurusan."</td>
              <td>".$data->nama_perusahaan."</td>
              <td>
                <a href= ".base_url("pembimbing/absensicontroller/detail/{$data->nis}")." > <button class='btn btn-block btn-success btn-xs' type='button' >Detail</button> </a>
              </td>
			          </tr>";
            }
          }else{
            echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
          }

          ?>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
