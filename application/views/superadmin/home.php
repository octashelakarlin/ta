
<!DOCTYPE html>
<html>
<?php $this->load->view('superadmin/head') ?>
<body class="hold-transition skin-yellow sidebar-mini">
<div class="wrapper">

  <?php $this->load->view('superadmin/header') ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php $this->load->view('superadmin/leftbar') ?>

  <!-- Content Wrapper. Contains page content -->
  <?php $this->load->view('superadmin/content') ?>
  <!-- /.content-wrapper -->
  <?php $this->load->view('superadmin/footer') ?>
</body>
</html>
