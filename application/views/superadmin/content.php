<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      <?php echo $header; ?>
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">

    <?php $this->load->view($content); ?>

  </section>
  <!-- /.content -->
</div>
