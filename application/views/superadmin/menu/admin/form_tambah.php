<div class="row">
  <div class="col-xs-12">
    <div class="box box-warning">
      <div class="box-header  with-border">
        <center><h3 class="box-title">Input Data Admin</h3></center>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <body>
          <div style="color: red;"><?php echo validation_errors(); ?></div>
          <?php echo form_open("superadmin/admincontroller/tambah"); ?>
            <table cellpadding="8">
              <tr>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"> Input Email User</label>
                  <input type="email" class="form-control" name="input_user_email" id="inputSuccess" placeholder="Enter Email User" value="<?php echo set_value('input_user_email'); ?>">
                </div>
              </tr>

              <tr>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"> Input Password User</label>
                  <input type="password" class="form-control" name="input_user_password" id="inputSuccess" placeholder="Enter Password User" value="<?php echo set_value('input_user_password'); ?>">
                </div>
              </tr>

              <tr>
                <!-- /.box -->
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"> Verifikasi</label>
                  <div class="box box-solid box-success">
                    <!-- /.box-header -->
                    <div class="box-body">
                      <div class="radio">
                        <label >
                          <input type="radio" name="input_user_verifikasi" id="optionsRadios1" value="0" <?php echo set_radio('verifikasi', '0'); ?>> Belum Aktif
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="input_user_verifikasi" id="optionsRadios2" value="1" <?php echo set_radio('verifikasi', '1'); ?>> Aktif
                        </label>
                      </div>
                    </div>
                    <!-- /.box-body -->

                  </div>
                  <!-- /.box -->

                </div>
              </tr>

            </table>


            <input type="submit" class="btn btn-block btn-success" name="submit" value="Simpan">
            <hr>
            <a href="<?php echo base_url('/superadmin/admincontroller/'); ?>"><input class="btn btn-block btn-danger" type="button" value="Batal"></a>
          <?php echo form_close(); ?>
        </body>

      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->


    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
