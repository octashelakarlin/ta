<div class="row">
  <div class="col-xs-12">
    <a href='<?php echo base_url("superadmin/admincontroller/tambah"); ?>'><input class="btn btn-block btn-success btn-xs" type="button" value="Tambah"></a>
    <div class="box box-solid box-warning">

      <div class="box-header">

        <center><h3 class="box-title">Daftar Admin</h3></center>

        <div class="box-tools">

        </div>
      </div>
      <!-- /.box-header -->

      <div class="box-body table-responsive no-padding">

        <table class="table table-striped">
          <tr>
            <th>ID</th>
            <th>Email</th>
            <th>Verifikasi</th>
            <th colspan="2">Aksi</th>
          </tr>
          <?php
          if( ! empty($admin)){ // Jika data siswa tidak sama dengan kosong, artinya jika data siswa ada
            foreach($admin as $data){
              echo "
              <tr>
                <td>".$data->user_id."</td>
                <td>".$data->user_email."</td>
                <td>";
                  if ($data->is_active == 1) {
                    echo 'Aktif';
                  }else {
                    echo "Belum Aktif";
                  }
                  echo "
                </td>";
                ?>
                <td><a href='<?php echo base_url("superadmin/admincontroller/ubah/{$data->user_id}"); ?>'>
  			           <button class='btn btn-block btn-success btn-xs' type='button' >Edit</button></a>
                </td>
                <?php echo form_open("superadmin/admincontroller/hapus/{$data->user_id}"); ?>
                <!-- <td><a href='".base_url("/superadmin/admincontroller/hapus/".$data->user_id)."'>
  			           <button class='btn btn-block btn-danger btn-xs' type='button' >Hapus</button></a>
                </td> -->
                <td>
                  <button class='btn btn-block btn-danger btn-xs' type="submit" name="hapus">Hapus</button>
                </td>
                <?php echo form_close(); ?>
              </tr>
            <?php
            }
          }else{ // Jika data siswa kosong
            echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
          }
          ?>
        </table>
      </div>
      <!-- /.box-body -->
    </div>

    <!-- /.box -->


    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
