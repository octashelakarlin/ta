<div class="row">
  <div class="col-xs-12">
   <a href='<?php echo base_url("admin/prakerincontroller/tambah"); ?>'><input class="btn btn-block btn-success btn-xs" type="button" value="Tambah"></a>
    <div class="box box-solid box-warning">
      <div class="box-header">
        <center><h3 class="box-title">Daftar Prakerin Siswa</h3></center>
      </div>

      <div class="box-body table-responsive no-padding">
        <table class="table table-striped">

          <tr>
            <th>NIS</th>
            <th>Nama Siswa</th>
      			<th>Jurusan</th>
      			<th>Pembimbing</th>
      			<th>Perusahaan</th>
            <th>Aksi</th>
          </tr>

          <?php
          if( ! empty($prakerin)){
            foreach($prakerin as $data){
              $id_prakerin = rawurlencode($data->id_prakerin);
              echo "<tr>
              <td>".$data->nis."</td>
              <td>".$data->nama_siswa."</td>
			         <td>".$data->nama_jurusan."</td>
			         <td>".$data->nama_pembimbing."</td>
			          <td>".$data->nama_perusahaan."</td>

              <td><a href='".base_url("/admin/prakerincontroller/ubah/".$id_prakerin)."'><button class='btn btn-block btn-success btn-xs' type='button' >Edit</button></a></td>
              <td><a href='".base_url("/admin/prakerincontroller/hapus/".$id_prakerin)."'><button class='btn btn-block btn-danger btn-xs' type='button' >Delete</button></a></td>
              </tr>";
            }
          }else{
            echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
          }
          ?>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
