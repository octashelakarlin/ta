<div class="row">
  <div class="col-xs-12">
    <div class="box box-warning">
      <div class="box-header  with-border">
        <center><h3 class="box-title">Input Data Prakerin</h3></center>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <body>
          <div style="color: red;"><?php echo validation_errors(); ?></div>
          <?php echo form_open("admin/prakerincontroller/tambah"); ?>
            <table cellpadding="8">


               <tr>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess">NIS</label>
                  <select name='input_nis' id='select-nis' required class="form-control">
                    <option value="" disabled selected>Pilih Siswa</option>
                    <?php
                     foreach ($siswa->result() as $data) {
                     echo "<option value='".$data->nis."'>".$data->nis."</option>";
                     }
                     ?>
                  </select>
                </div>
              </tr>

               <tr>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"> Input Nama Siswa</label>
                  <input type="text" class="form-control" name="input_nama_siswa" id="inputSuccess" placeholder=" Nama Siswa" value="<?php echo set_value('input_nama_siswa'); ?>">
 
                </div>
              </tr>
			  
			  <tr>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess">Jurusan</label>
                  <select name='input_id_jurusan' id='select-jurusan' required class="form-control">
                    <option value="" disabled selected>Pilih Jurusan</option>
                    <?php
                     foreach ($jurusan->result() as $data) {
                     echo "<option value='".$data->id_jurusan."'>".$data->nama_jurusan."</option>";
                     }
                     ?>
                  </select>
                </div>
              </tr>
			  

              <tr>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess">Pembimbing</label>
                  <select name='input_id_pembimbing' id='select-pembimbing' required class="form-control">
                    <option value="" disabled selected>Pilih Pembimbing</option>
                    <?php
                     foreach ($pembimbing->result() as $data) {
                     echo "<option value='".$data->id_pembimbing."'>".$data->nama_pembimbing."</option>";
                     }
                     ?>
                  </select>
                </div>
              </tr>
			  
			  <tr>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess">Perusahaan</label>
                  <select name='input_id_perusahaan' id='select-perusahaan' required class="form-control">
                    <option value="" disabled selected>Pilih Perusahaan</option>
                    <?php
                     foreach ($perusahaan->result() as $data) {
                     echo "<option value='".$data->id_perusahaan."'>".$data->nama_perusahaan."</option>";
                     }
                     ?>
                  </select>
                </div>
              </tr>
             
            </table>


            <input type="submit" class="btn btn-block btn-success" name="submit" value="Simpan">

            <hr>
            <a href="<?php echo base_url('/admin/prakerincontroller/'); ?>"><input class="btn btn-block btn-danger" type="button" value="Batal"></a>
          <?php echo form_close(); ?>
        </body>

      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->


    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
