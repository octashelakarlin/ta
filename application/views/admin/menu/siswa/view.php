<div class="row">
  <div class="col-xs-12">
    <div class="box box-solid box-warning">
      <div class="box-header">
        <center><h3 class="box-title">Daftar Siswa</h3></center>
      </div>

      <div class="box-body table-responsive no-padding">
        <table class="table table-striped">

          <tr>
            <th>NIS</th>
            <th>Nama Siswa</th>
			<th>Jurusan</th>
            <th hidden colspan="2">Aksi</th>
          </tr>

          <?php
          if( ! empty($siswa)){
            foreach($siswa as $data){
              $nis = rawurlencode($data->nis);
              echo "<tr>
              <td>".$data->nis."</td>
              <td>".$data->nama_siswa."</td>
			  <td>".$data->nama_jurusan."</td>

              <td hidden ><a href='".base_url("/admin/siswacontroller/ubah/".$nis)."'><button class='btn btn-block btn-success btn-xs' type='button' >Edit</button></a></td>
              <td hidden ><a href='".base_url("/admin/siswacontroller/hapus/".$nis)."'><button class='btn btn-block btn-danger btn-xs' type='button' >Delete</button></a></td>
              </tr>";
            }
          }else{
            echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
          }
          ?>
        </table>
      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
