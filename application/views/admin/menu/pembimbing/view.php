<div class="row">
  <div class="col-xs-12">
    <a href='<?php echo base_url("admin/pembimbingcontroller/tambah"); ?>'><input class="btn btn-block btn-success btn-xs" type="button" value="Tambah"></a>
    <div class="box box-solid box-warning">

      <div class="box-header">

        <center><h3 class="box-title">Daftar Pembimbing</h3></center>

        <div class="box-tools">

        </div>
      </div>
      <!-- /.box-header -->

      <div class="box-body table-responsive no-padding">

        <table class="table table-striped">
          <tr>
            <th>ID Pembimbing</th>
            <th>Nama Pembimbing</th>
            <th colspan="2">Aksi</th>
          </tr>
          <?php
          // $id_dosen_replace = str_replace(' ', '_', 'a a');
          if( ! empty($pembimbing)){ // Jika data siswa tidak sama dengan kosong, artinya jika data siswa ada
            foreach($pembimbing as $data){
              $encoded_id_pembimbing = rawurlencode($data->id_pembimbing);
              // $decoded_id_dosen = rawurldecode($encodedId);
              echo "<tr>
              <td>".$data->id_pembimbing."</td>
              <td>".$data->nama_pembimbing."</td>

              <td><a href='".base_url("/admin/pembimbingcontroller/ubah/".$encoded_id_pembimbing)."'><button class='btn btn-block btn-success btn-xs' type='button' >Edit</button></a></td>
              <td><a href='".base_url("/admin/pembimbingcontroller/hapus/".$encoded_id_pembimbing)."'><button class='btn btn-block btn-danger btn-xs' type='button' >Hapus</button></a></td>
              </tr>";
            }
          }else{ // Jika data siswa kosong
            echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
          }
          ?>
        </table>
      </div>
      <!-- /.box-body -->
    </div>

    <!-- /.box -->


    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
