<div class="row">
  <div class="col-xs-12">
    <div class="box box-warning">
      <div class="box-header  with-border">
        <center><h3 class="box-title">Update Data Perusahaan</h3></center>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <body>

          <div style="color: red;"><?php echo validation_errors(); ?></div>
          <?php echo form_open("admin/perusahaancontroller/ubah/".$perusahaan->id_perusahaan); ?>
            <table cellpadding="8">

          <tr>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"> Input ID Perusahaan</label>
                  <input type="text" class="form-control" name="input_id_perusahaan" id="inputSuccess" placeholder="ID Perusahaan" value="<?php echo set_value('input_id_perusahaan', $perusahaan->id_perusahaan); ?>" readonly>

                </div>
              </tr>

              <tr>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"> Input Nama Perusahaan</label>
                  <input type="text" class="form-control" name="input_nama_perusahaan" id="inputSuccess" placeholder="Nama Perusahaan" value="<?php echo set_value('input_nama_perusahaan', $perusahaan->nama_perusahaan); ?>">

                </div>
              </tr>
			  
			  <tr>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"> Alamat</label>
                  <input type="text" class="form-control" name="input_alamat" id="inputSuccess" placeholder="Alamat" value="<?php echo set_value('input_alamat', $perusahaan->alamat); ?>">

                </div>
              </tr>
			  
			 <tr>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess">Kuota</label>
                  <input type="text" class="form-control" name="input_kuota" id="inputSuccess" placeholder="Kuota" value="<?php echo set_value('input_kuota', $perusahaan->kuota); ?>">

                </div>
              </tr>

            </table>

            <hr>
            <input type="submit" class="btn btn-block btn-success" name="submit" value="Ubah">
            <hr>
            <a href="<?php echo base_url('/admin/perusahaancontroller/'); ?>"><input class="btn btn-block btn-danger" type="button" value="Batal"></a>
          <?php echo form_close(); ?>
        </body>
        </div>
        <!-- /.box-body -->
        </div>
        <!-- /.box -->


        <!-- /.box -->
        </div>
        <!-- /.col -->
        </div>
        <!-- /.row -->
