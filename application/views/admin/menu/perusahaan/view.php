<div class="row">
  <div class="col-xs-12">
    <a href='<?php echo base_url("admin/perusahaancontroller/tambah"); ?>'><input class="btn btn-block btn-success btn-xs" type="button" value="Tambah"></a>
    <div class="box box-solid box-warning">

      <div class="box-header">

        <center><h3 class="box-title">Daftar Perusahaan</h3></center>

        <div class="box-tools">

        </div>
      </div>
      <!-- /.box-header -->

      <div class="box-body table-responsive no-padding">

        <table class="table table-striped">
          <tr>
            <th>ID</th>
            <th>Nama Perusahaan</th>
			<th>Alamat</th>
			<th>Kuota</th>
            <th colspan="2">Aksi</th>
          </tr>
          <?php
          if( ! empty($perusahaan)){ // Jika data siswa tidak sama dengan kosong, artinya jika data siswa ada
            foreach($perusahaan as $data){
              echo "<tr>
              <td>".$data->id_perusahaan."</td>
              <td>".$data->nama_perusahaan."</td>
			  <td>".$data->alamat."</td>
			  <td>".$data->kuota."</td>
			  
              <td><a href='".base_url("/admin/perusahaancontroller/ubah/".$data->id_perusahaan)."'><button class='btn btn-block btn-success btn-xs' type='button' >Edit</button></a></td>
              <td><a href='".base_url("/admin/perusahaancontroller/hapus/".$data->id_perusahaan)."'><button class='btn btn-block btn-danger btn-xs' type='button' >Hapus</button></a></td>
              </tr>";
            }
          }else{ // Jika data siswa kosong
            echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
          }
          ?>
        </table>
      </div>
      <!-- /.box-body -->
    </div>

    <!-- /.box -->


    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
