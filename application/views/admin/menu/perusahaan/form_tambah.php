<div class="row">
  <div class="col-xs-12">
    <div class="box box-warning">
      <div class="box-header  with-border">
        <center><h3 class="box-title">Input Data Perusahaan</h3></center>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <body>
          <div style="color: red;"><?php echo validation_errors(); ?></div>
          <?php echo form_open("admin/perusahaancontroller/tambah"); ?>
            <table cellpadding="8">

              <tr>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"> Input Nama Perusahaan</label>
                  <input type="text" class="form-control" name="input_nama_perusahaan" id="inputSuccess" placeholder=" Nama Perusahaan" value="<?php echo set_value('input_nama_perusahaan'); ?>">
 
                </div>
              </tr>
			  
			<tr>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"> Alamat</label>
                  <input type="text" class="form-control" name="input_alamat" id="inputSuccess" placeholder="Alamat" value="<?php echo set_value('input_alamat'); ?>">

                </div>
              </tr>
			  
			 <tr>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess">Kuota</label>
                  <input type="text" class="form-control" name="input_kuota" id="inputSuccess" placeholder="Kuota" value="<?php echo set_value('input_kuota'); ?>">

                </div>
              </tr>

            </table>


            <input type="submit" class="btn btn-block btn-success" name="submit" value="Simpan">

            <hr>
            <a href="<?php echo base_url('/admin/perusahaancontroller/'); ?>"><input class="btn btn-block btn-danger" type="button" value="Batal"></a>
          <?php echo form_close(); ?>
        </body>

      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->


    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
