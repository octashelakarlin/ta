<div class="row">
  <div class="col-xs-12">
    <div class="box box-warning">
      <div class="box-header  with-border">
        <center><h3 class="box-title">Update Data Jurusan</h3></center>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <body>

          <div style="color: red;"><?php echo validation_errors(); ?></div>
          <?php echo form_open("admin/jurusancontroller/ubah/{$jurusan->id_jurusan}"); ?>
            <table cellpadding="8">


              <tr>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"> Input Nama Jurusan</label>
                  <input type="text" class="form-control" name="input_nama_jurusan" id="inputSuccess" placeholder="Enter Nama Jurusan" value="<?php echo set_value('input_nama_jurusan', $jurusan->nama_jurusan); ?>">

                </div>
              </tr>

            </table>

            <hr>
            <input type="submit" class="btn btn-block btn-success" name="submit" value="Ubah">
            <hr>
            <a href="<?php echo base_url('/admin/jurusancontroller/'); ?>"><input class="btn btn-block btn-danger" type="button" value="Batal"></a>
          <?php echo form_close(); ?>
        </body>
        </div>
        <!-- /.box-body -->
        </div>
        <!-- /.box -->


        <!-- /.box -->
        </div>
        <!-- /.col -->
        </div>
        <!-- /.row -->
