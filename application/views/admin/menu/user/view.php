<div class="row">
  <div class="col-xs-12">
     <a href='<?php echo base_url("admin/usercontroller/tambah"); ?>'><input class="btn btn-block btn-success btn-xs" type="button" value="Tambah"></a>
    <div class="box box-solid box-warning">

      <div class="box-header">

        <center><h3 class="box-title">Daftar User</h3></center>

        <div class="box-tools">

        </div>
      </div>
      <!-- /.box-header -->

      <div class="box-body table-responsive no-padding">

        <table class="table table-striped">
          <tr>
            <th>No</th>
            <th>Email</th>
            <th>User Level</th>
            <th>Hak Akses</th>
            <th colspan="2">Aksi</th>
          </tr>
          <?php
          $no=0;
          if( ! empty($user)){ // Jika data siswa tidak sama dengan kosong, artinya jika data siswa ada
            foreach($user as $data){
              echo "<tr>
              <td>".++$no."</td>
              <td>".$data->user_email."</td>
              <td>".$data->nama_user_level."</td>
              <td>".$data->hak_akses."</td>

              <td><a href='".base_url("/admin/usercontroller/ubah/".$data->user_id)."'><button class='btn btn-block btn-success btn-xs' type='button' >Edit</button></a></td>
              <td><a href='".base_url("/admin/usercontroller/hapus/".$data->user_id)."'><button class='btn btn-block btn-danger btn-xs' type='button' >Hapus</button></a></td>
              </tr>";
            }
          }else{ // Jika data siswa kosong
            echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
          }
          ?>
        </table>
      </div>
      <!-- /.box-body -->
    </div>

    <!-- /.box -->


    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
