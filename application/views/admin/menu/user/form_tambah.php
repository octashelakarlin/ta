<div class="row">
  <div class="col-xs-12">
    <div class="box box-warning">
      <div class="box-header  with-border">
        <center><h3 class="box-title">Input Data User</h3></center>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <body>
          <div style="color: red;"><?php echo validation_errors(); ?></div>
          <?php echo form_open("admin/usercontroller/tambah"); ?>
            <table cellpadding="8">


              <tr>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"> Input Email User</label>
                  <input autofocus type="email" class="form-control" name="input_user_email" id="inputSuccess" placeholder="Enter Email User" value="<?php echo set_value('input_user_email'); ?>">
                </div>
              </tr>

              <tr>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"> Input Password User</label>
                  <input type="password" class="form-control" name="input_user_password" id="inputSuccess" placeholder="Enter Password User" value="<?php echo set_value('input_user_password'); ?>">
                </div>
              </tr>

              <tr>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"> Input Level User</label>
                  <select name='input_user_level' id='select-level' class="form-control" required>
                    <option value="" disabled selected>Pilih Level</option>
                    <option value="Admin" >Admin</option>
                    <option value="Perusahaan" >Perusahaan</option>
                    <option value="Pembimbing" >Pembimbing</option>
                    <option value="Siswa" >Siswa</option>

                  <!-- <input type="text" class="form-control" name="input_user_level" id="inputSuccess" placeholder="Enter Level User" value="<?php echo set_value('input_user_level'); ?>"> -->
                </div>
              </tr>

              <tr>
                <div id="div-kode" class="form-group has-success">
                  <label class="control-label" for="inputSuccess">Kode Perusahaan</label>
                  <input type="text" class="form-control" name="input_id_perusahaan" id="inputSuccess" placeholder="Kode Perusahaan" value="<?php echo set_value('input_id_perusahaan'); ?>">
                </div>
              </tr>

              <tr>
                <div id="div-nip" class="form-group has-success">
                  <label class="control-label" for="inputSuccess"> Input NIP</label>
                  <input type="text" class="form-control" name="input_id_pembimbing" id="inputSuccess" placeholder="NIP" value="<?php echo set_value('input_id_pembimbing'); ?>">
                </div>
              </tr>

              <tr>
                <div id="div-nis" class="form-group has-success">
                  <label class="control-label" for="inputSuccess"> Input NIS</label>
                  <input type="text" class="form-control" name="input_id_siswa" id="inputSuccess" placeholder="NIS" value="<?php echo set_value('input_id_siswa'); ?>">
                </div>
              </tr>

              <tr>
                <!-- /.box -->
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"> Hak Akses</label>
                  <div class="box box-solid box-success">
                    <!-- /.box-header -->
                    <div class="box-body">
                      <div class="radio">
                        <label >
                          <input type="radio" name="input_hak_akses" id="optionsRadios1" value="0" <?php echo set_radio('hakakses', '0'); ?>> False
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="input_hak_akses" id="optionsRadios2" value="1" <?php echo set_radio('hakakses', '1'); ?>> True
                        </label>
                      </div>
                    </div>
                    <!-- /.box-body -->

                  </div>
                  <!-- /.box -->

                </div>
              </tr>

            </table>


            <input type="submit" class="btn btn-block btn-success" name="submit" value="Simpan">

            <hr>
            <a href="<?php echo base_url('/admin/usercontroller/'); ?>"><input class="btn btn-block btn-danger" type="button" value="Batal"></a>
          <?php echo form_close(); ?>

          <script src="<?php echo base_url("js/jquery.min.js"); ?>" type="text/javascript"></script>

          <script>
          $(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)
            $("#div-kode").hide();
            $("#div-nip").hide();
            $("#div-nis").hide();
            // Kita sembunyikan dulu untuk loadingnya
            // $("#loading").hide();

            $("#select-level").change(function(){ // Ketika user mengganti atau memilih data provinsi
              // $("#kota").hide(); // Sembunyikan dulu combobox kota nya
              // $("#loading").show(); // Tampilkan loadingnya
              var level = $("#select-level").val();
              if (level == "Admin")
              {
                $("#div-kode").hide();
                $("#div-nip").hide();
                $("#div-nis").hide();
              }
              else if (level == "Perusahaan")
              {
                $("#div-kode").show();
                $("#div-nip").hide();
                $("#div-nis").hide();
              }
              else if (level == "Pembimbing")
              {
                $("#div-kode").hide();
                $("#div-nip").show();
                $("#div-nis").hide();
              }
              else if (level == "Siswa")
              {
                $("#div-kode").hide();
                $("#div-nip").hide();
                $("#div-nis").show();
              }

            });
          });
          </script>

        </body>

      </div>
      <!-- /.box-body -->
    </div>
    <!-- /.box -->


    <!-- /.box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
