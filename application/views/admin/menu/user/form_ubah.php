<div class="row">
  <div class="col-xs-12">
    <div class="box box-warning">
      <div class="box-header  with-border">
        <center><h3 class="box-title">Update Data User</h3></center>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <body>

          <div style="color: red;"><?php echo validation_errors(); ?></div>
          <?php echo form_open("admin/usercontroller/ubah/".$user->user_id); ?>
            <table cellpadding="8">
              <tr>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"> Input Id User</label>
                  <input type="text" class="form-control" name="input_user_id" id="inputSuccess" placeholder="Enter Id User" value="<?php echo set_value('input_user_id', $user->user_id); ?>" readonly>
                </div>
              </tr>

              <tr>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"> Input Email User</label>
                  <input autofocus type="email" class="form-control" name="input_user_email" id="inputSuccess" placeholder="Enter Email User" value="<?php echo set_value('input_user_email', $user->user_email); ?>">
                </div>
              </tr>

              <tr>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"> Input Password User</label>
                  <input type="password" class="form-control" name="input_user_password" id="inputSuccess" placeholder="Enter Password User" value="<?php echo set_value('input_user_password', $user->user_password); ?>">
                </div>
              </tr>

              <tr>
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"> Input Level User</label>
                  <select name='input_user_level' id='select-level' class="form-control" required>
                    <option value="" disabled selected>Pilih Level</option>
                    <option value="<?php echo $user->user_level ?>" <?php if($user->nama_user_level == "Pembimbing"){ echo "selected";} ?>>Pembimbing</option>
                    <option value="<?php echo $user->user_level ?>" <?php if($user->nama_user_level == "Perusahaan"){ echo "selected";} ?>>Perusahaan</option>
                    <option value="<?php echo $user->user_level ?>" <?php if($user->nama_user_level == "Siswa"){ echo "selected";} ?>>Siswa</option>
                  </select>
                  <!-- <input hidden type="text" class="form-control" name="input_user_level" id="inputSuccess" placeholder="Enter Level User" value="<?php echo set_value('input_user_level', $user->nama_user_level); ?>"> -->
                </div>
              </tr>
<!--
              <?php

              if ($user->nama_user_level == 'Perusahaan') { ?>
                <tr>
                  <div id="div-kode" class="form-group has-success">
                  <label class="control-label" for="inputSuccess">Kode Perusahaan</label>
                  <input type="text" class="form-control" name="input_id_perusahaan" id="inputSuccess" placeholder="Kode Perusahaan" value="<?php echo set_value('input_id_perusahaan', $user->id_perusahaan); ?>">
                  </div>
                </tr><?php

              }elseif ($user->nama_user_level == 'pembimbing') { ?>
                <tr>
                  <div id="div-nip" class="form-group has-success">
                  <label class="control-label" for="inputSuccess">NIP</label>
                  <input type="text" class="form-control" name="input_id_pembimbing" id="inputSuccess" placeholder="Enter NIP" value="<?php echo set_value('input_id_pembimbing', $user->id_pembimbing); ?>">
                  </div>
                </tr><?php

              }elseif ($user->nama_user_level == 'Siswa') { ?>
                <tr>
                  <div id="div-nis" class="form-group has-success">
                  <label class="control-label" for="inputSuccess">NIS</label>
                  <input type="text" class="form-control" name="input_nis" id="inputSuccess" placeholder="NIS" value="<?php echo set_value('input_nis', $user->nis); ?>">
                  </div>
                </tr><?php

              }

               ?> -->

              <tr>
                <!-- /.box -->
                <div class="form-group has-success">
                  <label class="control-label" for="inputSuccess"> Hak Akses</label>
                  <div class="box box-solid box-success">
                    <!-- /.box-header -->
                    <div class="box-body">
                      <div class="radio">
                        <label >
                          <input type="radio" name="input_hak_akses" id="optionsRadios1" value="0" <?php echo set_radio('hakakses', '0', ($user->hak_akses == "0")? true : false); ?>> False
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="input_hak_akses" id="optionsRadios2" value="1" <?php echo set_radio('hakakses', '1', ($user->hak_akses == "1")? true : false); ?>> True
                        </label>
                      </div>
                    </div>
                    <!-- /.box-body -->

                  </div>
                  <!-- /.box -->

                </div>
              </tr>


            </table>

            <hr>
            <input type="submit" class="btn btn-block btn-success" name="submit" value="Ubah">
            <hr>
            <a href="<?php echo base_url('/admin/usercontroller/'); ?>"><input class="btn btn-block btn-danger" type="button" value="Batal"></a>
          <?php echo form_close(); ?>

          <script src="<?php echo base_url("js/jquery.min.js"); ?>" type="text/javascript"></script>

          <script>
          $(document).ready(function(){ // Ketika halaman sudah siap (sudah selesai di load)

            var level = $("#select-level").val();
            if (level == "Perusahaan")
            {
                $("#div-kode").show();
                $("#div-nip").hide();
                $("#div-nis").hide();

            }
            else if (level == "Pembimbing")
            {
                $("#div-nip").show();
                $("#div-kode").hide();
                $("#div-nis").hide();
            }
            else if (level == "Siswa")
            {
                $("#div-nis").show();
                $("#div-kode").hide();
                $("#div-nip").hide();
            }

            else if (level == "Admin")
            {
                $("#div-nim").hide();
                $("#div-nip").hide();
                $("#div-nis").hide();
            }
            // Kita sembunyikan dulu untuk loadingnya
            // $("#loading").hide();

            $("#select-level").change(function(){ // Ketika user mengganti atau memilih data provinsi
              // $("#kota").hide(); // Sembunyikan dulu combobox kota nya
              // $("#loading").show(); // Tampilkan loadingnya
              var level = $("#select-level").val();
              if (level == "Perusahaan")
              {
                  $("#div-kode").show();
                  $("#div-nip").hide();
                  $("#div-nis").hide();
              }
              else if (level == "Pembimbing")
              {
                  $("#div-nip").show();
                  $("#div-kode").hide();
                  $("#div-nis").hide();
              }
              else if (level == "Siswa")
              {
                  $("#div-nis").show();
                  $("#div-kode").hide();
                  $("#div-nip").hide();
              }

              else if (level == "Admin")
              {
                  $("#div-nim").hide();
                  $("#div-nip").hide();
                  $("#div-nis").hide();
              }

            });
          });
          </script>

        </body>
        </div>
        <!-- /.box-body -->
        </div>
        <!-- /.box -->


        <!-- /.box -->
        </div>
        <!-- /.col -->
        </div>
        <!-- /.row -->
