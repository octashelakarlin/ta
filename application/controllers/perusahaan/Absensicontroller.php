<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absensicontroller extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

	    $this->load->model('perusahaan/AbsensiModel');
	    $this->load->library('session');
	    $this->load->library('form_validation');

	    if ($this->session->userdata('user_level') != "Perusahaan") {
	      redirect('logincontroller');
	  }
	}

	public function index()
	{
	  $data['header'] = 'Data Absensi Siswa';
	  $data['content'] = 'perusahaan/menu/absensi/view';
	  $data['perusahaan'] = $this->AbsensiModel->view();
	  $this->load->view('perusahaan/home', $data);
	}

	public function tambah(){
    if($this->input->post('submit')){
      if($this->AbsensiModel->validation("save"))
      {
        $this->AbsensiModel->save();
        redirect('perusahaan/absensicontroller');
      }
    }

	$data['siswa'] = $this->AbsensiModel->getNis();
    $data['header'] = 'Data Absensi Siswa';
    $data['content'] = 'perusahaan/menu/absensi/form_tambah';
    $this->load->view('perusahaan/home', $data);
  }




}
