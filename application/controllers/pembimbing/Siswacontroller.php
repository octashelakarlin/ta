<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswacontroller extends CI_Controller {

  public function __construct(){
    parent::__construct();

    $this->load->model('pembimbing/SiswaModel'); // Load SiswaModel ke controller ini
    $this->load->library('session');

    if ($this->session->userdata('user_level') != "Pembimbing") {
      redirect('logincontroller');
    }

  }

  public function index()
	{
    $data['header']  = 'Daftar Siswa Bimbingan';
    $data['content'] = 'pembimbing/menu/daftarbimbingan';
    $data['siswa']   = $this->SiswaModel->view_by_pembimbing();
    // echo "<pre>";
    // print_r($data['siswa']);die();
    $this->load->view('pembimbing/home', $data);
	}


}
