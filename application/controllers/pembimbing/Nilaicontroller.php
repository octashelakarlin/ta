<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilaicontroller extends CI_Controller {

  public function __construct(){
    parent::__construct();

    $this->load->model('pembimbing/SiswaModel'); // Load SiswaModel ke controller ini
    $this->load->library('session');

    if ($this->session->userdata('user_level') != "Pembimbing") {
      redirect('logincontroller');
    }

  }

  public function index()
	{
    $data['header']  = 'Nilai Siswa';
    $data['content'] = 'pembimbing/menu/nilai';
    $data['siswa']   = $this->SiswaModel->get_nilai();
    // echo "<pre>";
    // print_r($data['siswa']);die();
    $this->load->view('pembimbing/home', $data);
	}

  // public function index()
  // {
  //   $this->load->view('upload_form', array('error' => ' ' ));
  // }
  //
  // public function do_upload()
  // {
  //   $config['upload_path']          = './uploads/';
  //   $config['allowed_types']        = 'gif|jpg|png';
  //   $config['max_size']             = 100;
  //   $config['max_width']            = 1024;
  //   $config['max_height']           = 768;
  //
  //   $this->load->library('upload', $config);
  //
  //   if ( ! $this->upload->do_upload('userfile'))
  //   {
  //           $error = array('error' => $this->upload->display_errors());
  //
  //           $this->load->view('upload_form', $error);
  //   }
  //   else
  //   {
  //           $data = array('upload_data' => $this->upload->data());
  //
  //           $this->load->view('upload_success', $data);
  //   }
  // }


}
