<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absensicontroller extends CI_Controller {

  public function __construct(){
    parent::__construct();

    $this->load->model('pembimbing/SiswaModel'); // Load SiswaModel ke controller ini
    $this->load->library('session');

    if ($this->session->userdata('user_level') != "Pembimbing") {
      redirect('logincontroller');
    }

  }

  public function index()
	{
    $data['header']  = 'Data Prakerin';
    $data['content'] = 'pembimbing/menu/absensi';
    $data['siswa']   = $this->SiswaModel->get_absensi();
    // echo "<pre>";
    // print_r($data['siswa']);die();
    $this->load->view('pembimbing/home', $data);
	}

  public function detail($nis){
    $data['header']  = 'Detail Prakerin';
    $data['content'] = 'pembimbing/menu/absensi_detail';
    $data['siswa']   = $this->SiswaModel->get_detail_absen($nis);
    // echo "<pre>";
    // print_r($data['siswa']);die();
    $this->load->view('pembimbing/home', $data);

  }


}
