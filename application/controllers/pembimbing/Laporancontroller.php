<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporancontroller extends CI_Controller {

  public function __construct(){
    parent::__construct();

    $this->load->model('pembimbing/SiswaModel'); // Load SiswaModel ke controller ini
    $this->load->library('session');

    if ($this->session->userdata('user_level') != "Pembimbing") {
      redirect('logincontroller');
    }

  }

  public function index()
	{
    $data['header']  = 'Data Laporan';
    $data['content'] = 'pembimbing/menu/laporan';
    $data['siswa']   = $this->SiswaModel->get_laporan();
    // echo "<pre>";
    // print_r($data['siswa']);die();
    $this->load->view('pembimbing/home', $data);
	}


}
