<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admincontroller extends CI_Controller {

  public function __construct(){
    parent::__construct();

    $this->load->model('superadmin/AdminModel'); // Load SiswaModel ke controller ini
    $this->load->library('session');

  }

  public function index()
	{
    $data['header']   = 'Data Admin';
    $data['content']  = 'superadmin/menu/admin/view';
    $data['admin']    = $this->AdminModel->view()->result();
    $this->load->view('superadmin/home', $data);
	}

  public function tambah(){
    // if($this->input->post('submit')){ // Jika user mengklik tombol submit yang ada di form
    //   if($this->AdminModel->validation("save")){ // Jika validasi sukses atau hasil validasi adalah TRUE
    //     $this->AdminModel->save(); // Panggil fungsi save() yang ada di SiswaModel.php
    //     redirect('superadmin/admincontroller');
    //   }
    // }


		// syarat form
    $this->form_validation->set_rules('input_user_email', 'Email', 'trim|required|max_length[30]');
    $this->form_validation->set_rules('input_user_password', 'Password', 'trim|required|max_length[100]');
    $this->form_validation->set_rules('input_user_verifikasi', 'User Verifikasi', 'trim|required');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

    $data['header'] = 'Data Admin';
    $data['content'] = 'superadmin/menu/admin/form_tambah';
    $this->load->view('superadmin/home', $data);

    if ($this->form_validation->run() == false) {
			// jika syarat form belum terpenuhi (tombol edit belum ditekan)
      $this->load->view('superadmin/home', $data);
    }else {
			// jika syarat pada form sudah terpenuhi (tombol edit sudah ditekan)
        $this->AdminModel->save(); // Panggil fungsi save() yang ada di SiswaModel.php
        redirect('superadmin/admincontroller');
    }
  }

  public function ubah($user_id){
    // if($this->AdminModel->validation("update")){ // Jika validasi sukses atau hasil validasi adalah TRUE
    //   $this->AdminModel->edit($user_id); // Panggil fungsi edit() yang ada di SiswaModel.php
    //   redirect('superadmin/admincontroller');
    // }

		// syarat form
    $this->form_validation->set_rules('input_user_email', 'Email', 'trim|required|max_length[30]');
    $this->form_validation->set_rules('input_user_password', 'Password', 'trim|required|max_length[100]');
    $this->form_validation->set_rules('input_user_verifikasi', 'User Verifikasi', 'trim|required');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

    $data['header']   = 'Data Admin';
    $data['content']  = 'superadmin/menu/admin/form_ubah';
    $data['admin']    = $this->AdminModel->view_by($user_id)->row();

    if ($this->form_validation->run() == false) {
			// jika syarat form belum terpenuhi (tombol edit belum ditekan)
      $this->load->view('superadmin/home', $data);
    }else {
			// jika syarat pada form sudah terpenuhi (tombol edit sudah ditekan)
      $this->AdminModel->edit($user_id); // Panggil fungsi edit() yang ada di SiswaModel.php
      redirect('superadmin/admincontroller');
    }
  }

  public function hapus($user_id){
    print_r($this->input->post('hapus'));
    if ( null !== $this->input->post('hapus') ) { // hapus isset maka hapus data disini
      $this->AdminModel->delete(0, $user_id);
      redirect('superadmin/admincontroller');
    }
    redirect('superadmin/admincontroller');
  }
}
