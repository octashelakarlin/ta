<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prakerincontroller extends CI_Controller {

  public function __construct(){
    parent::__construct();

    $this->load->model('admin/PrakerinModel'); // Load TugasakhirModel ke controller ini
    $this->load->library('session');

    if ($this->session->userdata('is_login')!="1") {
      redirect('logincontroller');
    }
  }


	public function index()
	{
		$data['header']   = 'Data Prakerin';
		$data['content']  = 'admin/menu/prakerin/view';
		$data['prakerin'] = $this->PrakerinModel->view();
		$this->load->view('admin/home', $data);
	}

	public function tambah()
	{
		if($this->input->post('submit')){ // Jika user mengklik tombol submit yang ada di form
		  if($this->PrakerinModel->validation("save")){ // Jika validasi sukses atau hasil validasi adalah TRUE
			$this->PrakerinModel->save(); // Panggil fungsi save() yang ada di TugasakhirModel.php
			redirect('admin/prakerincontroller');
		  }
		}
		$data['siswa']      = $this->PrakerinModel->getNis();
		$data['pembimbing'] = $this->PrakerinModel->getPembimbing();
		$data['perusahaan'] = $this->PrakerinModel->getPerusahaan();
		$data['jurusan']    = $this->PrakerinModel->getJurusan();


		$data['header']   = 'Data Prakerin';
		$data['content']  = 'admin/menu/prakerin/form_tambah';
		$this->load->view('admin/home', $data);
  }

  public function ubah($id_prakerin)
  {
	  if($this->input->post('submit'))
	  {
     	if($this->PrakerinModel->validation("update"))
        $this->PrakerinModel->edit($id_prakerin);
        redirect('admin/prakerincontroller');
    }
    $data['jurusan']    = $this->PrakerinModel->getJurusan();
  	$data['pembimbing'] = $this->PrakerinModel->getPembimbing();
  	$data['perusahaan'] = $this->PrakerinModel->getPerusahaan();

    $data['header']     = 'Data Prakerin';
    $data['content']    = 'admin/menu/prakerin/form_ubah';

	$data['prakerin']     = $this->PrakerinModel->view_by($id_prakerin);
    $this->load->view('admin/home', $data);
  }

	public function hapus($id_prakerin){
    $this->PrakerinModel->delete($id_prakerin); // Panggil fungsi delete() yang ada di SiswaModel.php
    redirect('admin/prakerincontroller');
  }


}
