<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usercontroller extends CI_Controller {

  public function __construct(){
    parent::__construct();

    $this->load->model('admin/UserModel'); // Load SiswaModel ke controller ini
    $this->load->library('session');

    if ($this->session->userdata('is_login')!="1") {
      redirect('logincontroller');
    }
  }

  public function index()
	{
    // echo "<pre>";
    // print_r($this->session->userdata());die();
    $data['header'] = 'Data User';
    $data['content'] = 'admin/menu/user/view';
    $data['user'] = $this->UserModel->view()->result();
    $this->load->view('admin/home', $data);
	}

   public function tambah(){
    if($this->input->post('submit')){ // Jika user mengklik tombol submit yang ada di form
      if($this->UserModel->validation("save")){ // Jika validasi sukses atau hasil validasi adalah TRUE
        $this->UserModel->save(); // Panggil fungsi save() yang ada di SiswaModel.php
        redirect('admin/usercontroller');
      }
    }
    $data['header'] = 'Data User';
    $data['content'] = 'admin/menu/user/form_tambah';
    $this->load->view('admin/home', $data);
  }


  public function ubah($user_id){
    if($this->input->post('submit')){ // Jika user mengklik tombol submit yang ada di form
      if($this->UserModel->validation("update")){ // Jika validasi sukses atau hasil validasi adalah TRUE
        $this->UserModel->edit($user_id); // Panggil fungsi edit() yang ada di SiswaModel.php
        redirect('admin/usercontroller');
      }
    }
    $data['header'] = 'Data User';
    $data['content'] = 'admin/menu/user/form_ubah';
    $data['user'] = $this->UserModel->view_by($user_id);
    $this->load->view('admin/home', $data);
  }

  public function hapus($user_id){
    $this->UserModel->delete($user_id); // Panggil fungsi delete() yang ada di SiswaModel.php
    redirect('admin/usercontroller');
  }
}
