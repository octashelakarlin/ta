<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perusahaancontroller extends CI_Controller {

  public function __construct(){
    parent::__construct();

    $this->load->model('admin/PerusahaanModel'); // Load SiswaModel ke controller ini
    $this->load->library('session');

    if ($this->session->userdata('is_login')!="1") {
      redirect('logincontroller');
    }
  }

  public function index()
	{
    $data['header'] = 'Data Perusahaan';
    $data['content'] = 'admin/menu/perusahaan/view';
    $data['perusahaan'] = $this->PerusahaanModel->view();
    $this->load->view('admin/home', $data);
	}

  public function tambah(){
    if($this->input->post('submit')){ // Jika user mengklik tombol submit yang ada di form
      if($this->PerusahaanModel->validation("save")){ // Jika validasi sukses atau hasil validasi adalah TRUE
        $this->PerusahaanModel->save(); // Panggil fungsi save() yang ada di SiswaModel.php
        redirect('admin/perusahaancontroller');
      }
    }
    $data['header'] = 'Data Perusahaan';
    $data['content'] = 'admin/menu/perusahaan/form_tambah';
    $this->load->view('admin/home', $data);
  }

  public function hapus($id_perusahaan){
    $this->PerusahaanModel->delete($id_perusahaan); // Panggil fungsi delete() yang ada di SiswaModel.php
    redirect('admin/perusahaancontroller');
  }

  public function ubah($id_perusahaan){
    if($this->input->post('submit')){ // Jika user mengklik tombol submit yang ada di form
      if($this->PerusahaanModel->validation("update")){ // Jika validasi sukses atau hasil validasi adalah TRUE
        $this->PerusahaanModel->edit($id_perusahaan); // Panggil fungsi edit() yang ada di SiswaModel.php
        redirect('admin/perusahaancontroller');
      }
    }
    $data['header'] = 'Data Perusahaan';
    $data['content'] = 'admin/menu/perusahaan/form_ubah';
    $data['perusahaan'] = $this->PerusahaanModel->view_by($id_perusahaan);
    $this->load->view('admin/home', $data);
  }
}
