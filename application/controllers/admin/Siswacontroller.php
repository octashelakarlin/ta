<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswacontroller extends CI_Controller {

  public function __construct(){
    parent::__construct();

    $this->load->model('admin/SiswaModel'); // Load SiswaModel ke controller ini
    $this->load->library('session');

    if ($this->session->userdata('is_login') != "1") {
      redirect('logincontroller');
    }
  }

  public function index()
	{
    $data['header'] = 'Data Siswa';
    $data['content'] = 'admin/menu/siswa/view';
    $data['siswa'] = $this->SiswaModel->view();
    $this->load->view('admin/home', $data);
	}
}
