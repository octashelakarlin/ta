<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daftarcontroller extends CI_Controller {

  public function __construct(){
    parent::__construct();

      $this->load->model('DaftarModel'); // Load SiswaModel ke controller ini
      $this->load->library('session'); // Load SiswaModel ke controller ini
  }

  private function _validasiLogin(){
		if ( $this->session->userdata('is_login') == 1 ) {
      if ($this->session->userdata('is_superadmin')) {
        redirect(base_url('superadmin/admincontroller'));
      }elseif ($this->session->userdata('is_admin')) {
        redirect(base_url('admin/jurusancontroller'));
      }elseif ($this->session->userdata('is_siswa')) {
        redirect(base_url('siswa/siswacontroller'));
      }elseif ($this->session->userdata('is_pembimbing')) {
        redirect(base_url('pembimbing/siswacontroller'));
      }

		}
	}

  public function index()
	{
    $this->_validasiLogin();
    $this->load->view('daftar');
	}

  public function proses_daftar()
  {
    $this->_validasiLogin();
    $nis = "";
    $id_pembimbing = "";

    $user_email = $this->input->post('input_user_email');
    $user_password = $this->input->post('input_user_password');
    $user_level = $this->input->post('input_user_level');
    $nis = $this->input->post('input_nis');
    $id_pembimbing = $this->input->post('input_id_pembimbing');

    $cekdaftar = $this->DaftarModel->daftar($user_email,$user_password,$user_level,$nis,$id_pembimbing);

    if ($user_level == "Admin") {
      if ($cekdaftar) {

        $data['pesan']="Daftar Akun Berhasil, Silahkan Login.";
        $this->load->view('login',$data);

      } else {
        $data['pesan']="Daftar Akun Gagal, Email sudah terdaftar, Silahkan coba email lain!!!.";
        $this->load->view('daftar',$data);
      }

    } elseif ($user_level == "Siswa") {
      if ($cekdaftar) {

        $data['pesan']="Daftar Akun Berhasil, Silahkan Login.";
        $this->load->view('login',$data);

      } else {
        $data['pesan']="Daftar Akun Gagal, Email atau NIS Sudah Terdaftar, Silahkan hubungi Admin!!!.";
        $this->load->view('daftar',$data);
      }

    }

}
}
