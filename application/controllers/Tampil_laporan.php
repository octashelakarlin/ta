<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tampil_laporan extends CI_Controller {

	// public function index()
	// {
	// 	$this->load->view('welcome_message');
	// }

  public function pdf_docx($fileName)
	{
    if ( $fileName == '' ) {
      redirect(base_url());
    }
    $file = file_get_contents( FCPATH . "assets/file/laporan/{$fileName}" );
    $file = FCPATH . "assets/file/laporan/{$fileName}";
    // header('Content-Type: application/pdf');
    // echo $file;
    header("Content-type: application/pdf");
    header("Content-Disposition: inline; filename={$fileName}");
    @readfile($file);

    // header("Content-Length: " . filesize ('assets/file/laporan/'.$fileName ) );
    // header("Content-type: application/pdf");
    // header("Content-disposition: inline; filename=".basename('assets/file/laporan/'.$fileName));
    // header('Expires: 0');
    // header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
    // $filepath = readfile('assets/file/laporan/'.$fileName);
    //
    // echo $filepath;

	}
}
