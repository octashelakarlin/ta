<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembimbingcontroller extends CI_Controller {

  public function __construct(){
    parent::__construct();

    $this->load->model('siswa/PembimbingModel'); // Load SiswaModel ke controller ini
    $this->load->library('session');

    if ($this->session->userdata('user_level')!="Siswa") {
      redirect('logincontroller');
    }
  }

  public function index()
	{
    $data['header'] = 'Data Pembimbing';
    $data['content'] = 'siswa/menu/pembimbing/view';
    $data['pembimbing'] = $this->PembimbingModel->view();
    $this->load->view('siswa/home', $data);
	}

  public function search()
  {
    $keyword = $this->input->post('keyword');
    $data['header'] = 'Data Pembimbing';
    $data['content'] = 'siswa/menu/pembimbing/view';
    $data['pembimbing'] = $this->PembimbingModel->getPembimbing($keyword);
    $this->load->view('siswa/home',$data);
  }
}
