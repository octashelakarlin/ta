<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prakerincontroller extends CI_Controller {

  public function __construct(){
    parent::__construct();

    $this->load->model('siswa/PrakerinModel'); // Load SiswaModel ke controller ini
    $this->load->library('session');

    if ($this->session->userdata('is_login') != "1") {
      redirect('logincontroller');
    }
  }

  public function index()
	{
    $data['header']     = 'Data Prakerin';
    $data['content']    = 'siswa/menu/prakerin/view';
    $data['prakerin']   = $this->PrakerinModel->view();
    $this->load->view('siswa/home', $data);
	}

  public function search()
  {
    $keyword = $this->input->post('keyword');
    $data['header']     = 'Data Prakerin';
    $data['content']    = 'siswa/menu/prakerin/view';
    $data['prakerin']   = $this->PrakerinModel->getPrakerin($keyword);
    $this->load->view('siswa/home',$data);
  }
}
