<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswacontroller extends CI_Controller {

  public function __construct(){
    parent::__construct();

    $this->load->model('siswa/SiswaModel'); // Load TugasakhirModel ke controller ini
    $this->load->library('session');
    $this->load->library('form_validation');

    if ($this->session->userdata('is_login')!="1") {
      redirect('logincontroller');
    }

    $nis = $this->session->userdata('nis');
  }

  public function index()
  {
    $data['header']   = 'Data Siswa';
    $data['content']  = 'siswa/menu/siswa/view';
    $data['siswa']    = $this->SiswaModel->get_siswa($this->session->userdata('nis'));
    $this->load->view('siswa/home', $data);
  }

  public function tambah(){
    if($this->input->post('submit')){ // Jika user mengklik tombol submit yang ada di form
      if($this->SiswaModel->validation("save")){ // Jika validasi sukses atau hasil validasi adalah TRUE
        $this->SiswaModel->save(); // Panggil fungsi save() yang ada di SiswaModel.php
        redirect('siswa/siswacontroller');
      }
    }

	  $data['jurusan']   = $this->SiswaModel->getJurusan();
    $data['header']    = 'Data Siswa';
    $data['content']   = 'siswa/menu/siswa/form_tambah';
    $this->load->view('siswa/home', $data);
  }


    public function ubah($nis){
    // $id_jurusan = $this->uri->segment(5);
    if($this->input->post('submit')){ // Jika mahasiswa mengklik tombol submit yang ada di form
      if($this->SiswaModel->validation("update")){ // Jika validasi sukses atau hasil validasi adalah TRUE
        $this->SiswaModel->edit($nis); // Panggil fungsi edit() yang ada di SiswaModel.php

        $this->session->set_userdata('nis',           $this->input->post('input_nis'));
        $this->session->set_userdata('nama_siswa',    $this->input->post('input_nama_siswa'));
        $this->session->set_userdata('tanggal_lahir', $this->input->post('input_tanggal_lahir'));
        $this->session->set_userdata('alamat',        $this->input->post('input_alamat'));
        $this->session->set_userdata('jenis_kelamin', $this->input->post('input_jenis_kelamin'));
        $this->session->set_userdata('id_jurusan',    $this->input->post('input_id_jurusan'));
        // $this->session->set_userdata('nama_jurusan', $this->input->post('input_id_jurusan'));
        redirect('siswa/siswacontroller');
      }
    }

    $data['jurusan']  = $this->SiswaModel->getJurusan();
    $data['header']   = 'Data Siswa';
    $data['content']  = 'siswa/menu/siswa/form_ubah';
    $data['siswa']    = $this->SiswaModel->get_siswa($nis);
    $this->load->view('siswa/home', $data);
  }

}
