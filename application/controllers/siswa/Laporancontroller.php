<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporancontroller extends CI_Controller {

	public function __construct(){
    parent::__construct();

    $this->load->model('siswa/LaporanModel'); // Load TugasakhirModel ke controller ini
    $this->load->library('session');
    $this->load->library('form_validation');

    if ($this->session->userdata('is_login')!="1") {
      redirect('logincontroller');
    }

    $id_laporan = $this->session->userdata('id_laporan');
  }

  public function index()
  {
		$data['header'] 					= 'Data Laporan';
    $data['content'] 					= 'siswa/menu/laporan/view';
		$data['field_laporan']		= $this->LaporanModel->get_laporan()[0];
    $data['laporan'] 					= $this->LaporanModel->get_laporan();
		// echo "<pre>";
		// print_r($data);die();
    $this->load->view('siswa/home', $data);

  }

   public function tambah(){
    if($this->input->post('submit')){ // Jika user mengklik tombol submit yang ada di form
      if($this->LaporanModel->validation("save")){ // Jika validasi sukses atau hasil validasi adalah TRUE
        $this->LaporanModel->save(); // Panggil fungsi save() yang ada di SiswaModel.php
        redirect('siswa/laporancontroller');
      }
    }

    $data['header'] 		= 'Data Laporan';
    $data['content'] 		= 'siswa/menu/laporan/form_tambah';
    $data['laporan'] 		= $this->LaporanModel->get_laporan()[0];
    $this->load->view('siswa/home', $data);
  }

	public function upload_file(){
		$target_dir = "assets/file/laporan";
		$target_file = $target_dir . basename($_FILES["laporan"]["name"]);
		echo "<pre>";
		print_r($_FILES);die();
		$uploadOk = 1;
		$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
		// Check if image file is a actual image or fake image
		if(isset($_POST["submit"])) {
		    $check = getimagesize($_FILES["laporan"]["tmp_name"]);
		    if($check !== false) {
		        echo "File is an image - " . $check["mime"] . ".";
		        $uploadOk = 1;
		    } else {
		        echo "File is not an image.";
		        $uploadOk = 0;
		    }
		}
		// Check if file already exists
		if (file_exists($target_file)) {
		    echo "Sorry, file already exists.";
		    $uploadOk = 0;
		}
		// Check file size
		if ($_FILES["laporan"]["size"] > 500000) {
		    echo "Sorry, your file is too large.";
		    $uploadOk = 0;
		}
		// Allow certain file formats
		if($imageFileType != "doc" && $imageFileType != "docx" && $imageFileType != "pdf"
		&& $imageFileType != "xlsx" ) {
		    echo "Sorry, only DOC, DOCX, PDF & XLSX files are allowed.";
		    $uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
		if ($uploadOk == 0) {
		    echo "Sorry, your file was not uploaded.";
		// if everything is ok, try to upload file
		} else {
		    if (move_uploaded_file($_FILES["laporan"]["tmp_name"], $target_file)) {
		        echo "The file ". basename( $_FILES["laporan"]["name"]). " has been uploaded.";
		    } else {
		        echo "Sorry, there was an error uploading your file.";
		    }
		}
	}

	public function upload_file2()
	{

		$config['upload_path']    = './assets/file/laporan/';
		$config['allowed_types']  = 'doc|docx|pdf';
		// $config['file_name']      = $nmfile;
		$this->load->library('upload', $config);

		if ( !$this->upload->do_upload('userfile')) {

			$this->form_validation->set_error_delimiters('<p class="error">', '</p>');

			$data['pesan'] 		= $this->upload->display_errors();
			$data['header'] 	= 'Data Laporan';
			$data['content'] 	= 'siswa/menu/laporan/view';
			// $data['laporan'] 	= $this->LaporanModel->view($this->session->userdata('nis'));
			redirect('siswa/laporancontroller');
			$this->load->view('siswa/home', $data);
		}
		else {
			$return 	= 	array(
											'result' 	=> 'success',
											'file' 		=> $this->upload->data(),
											'error' 	=> ''
										);

			$this->SiswaModel->edit_path($return,$id_laporan);

			$data['pesan'] 		= 'Upload Succes'; //$this->upload->data();
			$data['header'] 	= 'Data Laporan';
			$data['content'] 	= 'siswa/menu/laporan/view';
			$data['laporan'] 	= $this->LaporanModel->view($this->session->userdata('nis'));
			$this->load->view('siswa/home', $data);
		}

	}


}
