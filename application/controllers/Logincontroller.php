<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Logincontroller extends CI_Controller {
  public function __construct(){
    parent::__construct();
    $this->load->model('LoginModel'); // Load TugasakhirModel ke controller ini
    $this->load->library('session');
  }

  private function _validasiLogin(){
		if ( $this->session->userdata('is_login') == 1 ) {
      if ($this->session->userdata('is_superadmin')) {
        // echo "<pre>";
        // print_r('$user_level');
        redirect(base_url('superadmin/admincontroller'));

      }elseif ($this->session->userdata('is_admin')) {
        redirect(base_url('admin/jurusancontroller'));

      }elseif ( $this->session->userdata('is_siswa') == 1) {
        redirect(base_url('siswa/siswacontroller'));

      }elseif ( ($this->session->userdata('is_pembimbing')) == 1 ) {
        redirect(base_url('pembimbing/siswacontroller'));

      }elseif ( ($this->session->userdata('is_perusahaan')) == 1 ) {
        redirect(base_url('perusahaan/absensicontroller'));

      }

		}
	}

  public function index()
	{
    $this->_validasiLogin();
	  $this->load->view('login');
	}

  public function proses_login()
  {
    $this->_validasiLogin();
    $user_email     = $this->input->post('input_user_email');
    $user_password  = $this->input->post('input_user_password');
    $user_level     = $this->input->post('input_level');

    $ceklogin       = $this->LoginModel->login($user_email, $user_password, $user_level);

    if ($ceklogin == false) { //kalo data loginnya salah jadi gabisa masuk
      redirect('logincontroller');
    }else { // kalo bener
      $ceklogin = $ceklogin->row();
    }

    if ($user_level == "Superadmin") {
      if($ceklogin) {
          $this->session->set_userdata('user_id', $ceklogin->user_id);
          $this->session->set_userdata('nama_superadmin', $ceklogin->nama_superadmin);
          $this->session->set_userdata('user_email', $ceklogin->user_email);
          $this->session->set_userdata('user_password', $ceklogin->user_password);
          $this->session->set_userdata('user_level', $ceklogin->nama_user_level);
          $this->session->set_userdata('hak_akses', $ceklogin->hak_akses);
          $this->session->set_userdata('is_active', $ceklogin->is_active);
          $this->session->set_userdata('is_superadmin', 1);
          $this->session->set_userdata('is_login', 1);
          redirect('superadmin/admincontroller');

      } else {
        $data['pesan'] = "Email dan Password salah";
        $this->load->view('login',$data);
		  }

	  } elseif ( $user_level == "Admin" ) {
      if ( $ceklogin ) {
        if ( $ceklogin->is_active=='1' ) {
          $this->session->set_userdata('user_id', $ceklogin->user_id);
          $this->session->set_userdata('nama_admin', $ceklogin->nama_admin);
          $this->session->set_userdata('user_email', $ceklogin->user_email);
          $this->session->set_userdata('user_password', $ceklogin->user_password);
          $this->session->set_userdata('user_level', $ceklogin->nama_user_level);
          $this->session->set_userdata('hak_akses', $ceklogin->hak_akses);
          $this->session->set_userdata('is_active', $ceklogin->is_active);
          $this->session->set_userdata('is_admin', 1);
          $this->session->set_userdata('is_login', 1);
          redirect('admin/jurusancontroller');

        } else {
          $data['pesan'] = "Akun Anda belum diverifikasi, harap menghubungi Superadmin";
          $this->load->view('login',$data);
        }

      } else {
        $data['pesan'] = "Email dan Password salah";
        $this->load->view('login',$data);
      }

    } elseif ($user_level == "Siswa" || $user_level == "Pembimbing" || $user_level == "Perusahaan") {
      if ($ceklogin) {
        $this->session->set_userdata('user_id', $ceklogin->user_id);
        $this->session->set_userdata('user_email', $ceklogin->user_email);
        $this->session->set_userdata('user_password', $ceklogin->user_password);
        $this->session->set_userdata('user_level', $ceklogin->nama_user_level);
        $this->session->set_userdata('hak_akses', $ceklogin->hak_akses);
        $this->session->set_userdata('is_active', $ceklogin->is_active);
        $this->session->set_userdata('is_login', 1);

        if(isset($ceklogin->nama_siswa)){
            $this->session->set_userdata('nis', $ceklogin->nis);
            $this->session->set_userdata('nama_siswa', $ceklogin->nama_siswa);
            $this->session->set_userdata('tanggal_lahir', $ceklogin->tanggal_lahir);
            $this->session->set_userdata('alamat', $ceklogin->alamat);
            $this->session->set_userdata('jenis_kelamin', $ceklogin->jenis_kelamin);
            $this->session->set_userdata('nama_jurusan', $ceklogin->nama_jurusan);
            $this->session->set_userdata('is_siswa', 1);
            redirect('siswa/siswacontroller');

        } elseif(isset($ceklogin->nama_pembimbing)){
            $this->session->set_userdata('nama_pembimbing', $ceklogin->nama_pembimbing);
            $this->session->set_userdata('nama_jurusan', $ceklogin->nama_jurusan);
            $this->session->set_userdata('is_pembimbing', 1);
            redirect('pembimbing/siswacontroller');

        } elseif(isset($ceklogin->nama_perusahaan)){
            $this->session->set_userdata('nama_perusahaan', $ceklogin->nama_perusahaan);
            $this->session->set_userdata('alamat', $ceklogin->alamat);
            $this->session->set_userdata('kuota', $ceklogin->kuota);
            $this->session->set_userdata('is_perusahaan', 1);
            redirect('perusahaan/absensicontroller');
        }

        if ($this->session->userdata('hak_akses') == '0') {
          $data['pesan'] = "Akun Anda belum diverifikasi, Harap Menghubungi Admin.";
          $this->load->view('login',$data);
        }

      } else{
        $data['pesan']="Email dan Password $user_level tidak sesuai.";
        $this->load->view('login',$data);
      }
    }

  }




  function logout(){
    $this->session->sess_destroy();
    redirect('logincontroller');
  }
}
