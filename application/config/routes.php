<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'logincontroller';
$route['file/(:any)'] = 'tampil_laporan/pdf_docx/$1';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
